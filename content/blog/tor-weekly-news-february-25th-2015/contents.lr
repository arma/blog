title: Tor Weekly News — February 25th, 2015
---
pub_date: 2015-02-25
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the eighth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.6-alpha-3 is out</h1>

<p>Nick Mathewson <a href="https://blog.torproject.org/blog/tor-0263-alpha-released" rel="nofollow">announced</a> the third (“and hopefully final”) alpha release in the Tor 0.2.6.x series. The major user- and operator-facing changes in this release include support for AF_UNIX sockets, allowing high-risk applications to reach a local Tor without having to enable other kinds of networking activity; a new warning for relay operators, in order to make it even harder to accidentally run an exit node; improvements and additions to the directory system; and much else besides.</p>

<p>See Nick’s announcement for the full changelog, and download your copy of the source code from the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>, but note that “this is an alpha release”, so “please expect bugs”.</p>

<h1>Tails 1.3 is out</h1>

<p>The Tails team <a href="https://tails.boum.org/news/version_1.3/" rel="nofollow">announced</a> version 1.3 of the anonymous live operating system. This release brings several major new features, including Bitcoin support with Electrum; connections to Tor using the obfs4 pluggable transport; an AppArmor profile to protect the filesystem against some kinds of attack on Tor Browser; a simpler Tails drive creation process on Mac and Linux; and more intuitive handling of computer trackpads.</p>

<p>See the announcement for links to the full list of changes and known issues, and download your copy from the <a href="https://tails.boum.org/download/" rel="nofollow">website</a> or, if you already have a running Tails, using the incremental update system.</p>

<h1>CITIZENFOUR wins many awards</h1>

<p>Laura Poitras’ documentary film <a href="https://citizenfourfilm.com/" rel="nofollow">CITIZENFOUR</a>, recording the initial encounters between herself, journalist Glenn Greenwald, and the American surveillance whistleblower (and sometime <a href="http://www.wired.com/2014/05/snowden-cryptoparty/" rel="nofollow">Tor relay operator</a>) Edward Snowden, has been decorated at numerous awards ceremonies over the <a href="https://en.wikipedia.org/wiki/Citizenfour#Awards_and_nominations" rel="nofollow">past few months</a> for its artistic and political achievement.</p>

<p>The filmmakers have been tireless in their activism on behalf of Tor and the free software community both in the mainstream press and at <a href="https://media.ccc.de/browse/congress/2014/31c3_-_6258_-_en_-_saal_1_-_201412282030_-_reconstructing_narratives_-_jacob_-_laura_poitras.html" rel="nofollow">community conferences</a>; upon <a href="http://www.nationinstitute.org/blog/prizes/4376/%22citizenfour%22_will_receive_the_ridenhour_documentary_film_prize/" rel="nofollow">receiving the Ridenhour Documentary Film Prize</a> last week, Laura called attention to the role of these projects in the production process: “This film and our NSA reporting would not have been possible without the work of the Free Software community that builds free tools to communicate privately. The prize money for the award will be given to the Tails Free Software project.”</p>

<p>CITIZENFOUR went on to win the <a href="http://www.filmindependent.org/press/press-releases/2015-film-independent-spirit-awards-winners-announced/" rel="nofollow">Independent Spirit</a> and <a href="http://www.oscars.org/oscars/ceremonies/2015" rel="nofollow">Academy</a> Awards for Best Documentary Feature over the weekend. The recognition is richly deserved. Thanks to Laura and her colleagues for their extraordinary work over the last two years!</p>

<h1>Miscellaneous news</h1>

<p>Giovanni Pellerano <a href="https://lists.torproject.org/pipermail/tor-talk/2015-February/036969.html" rel="nofollow">released</a> a security advisory for a bug in GlobaLeaks that was introduced on 28th January 2015, and fixed on 16th February. The bug could have allowed an attacker to read any file in the /var/globaleaks/ directory with the exception of the Tor onion service key; if you installed or upgraded your GlobaLeaks instance on or between those dates, please see Giovanni’s announcement for more details, and upgrade again as soon as possible.</p>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-February/004261.html" rel="nofollow">announced</a> Orbot version 15-alpha-4, featuring bridge scanning and distribution via QR code, and simpler configuration for pluggable transports like meek, among other improvements.</p>

<p>Rob Jansen <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008317.html" rel="nofollow">announced</a> a major new release of <a href="https://shadow.github.io/" rel="nofollow">Shadow</a>, the Tor network simulation tool. New features include support for running Bitcoin software inside the simulation, client activity modelling using dependency graphs, and much more.</p>

<p>Yaron Goland <a href="https://lists.torproject.org/pipermail/tor-talk/2015-February/036952.html" rel="nofollow">updated</a> the <a href="https://github.com/thaliproject/Tor_Onion_Proxy_Library" rel="nofollow">Tor Onion Proxy libary</a>, a project to “enable Android and Java applications to easily host their own Tor Onion Proxies using the core Tor binaries”. This release incorporates newer software and a simplified build process.</p>

<p>The organizers of the Workshop on Surveillance and Technology issued a <a href="https://satsymposium.org/" rel="nofollow">call for papers</a> ahead of their event, which will be held on June 29th. The deadline for submission is midnight UTC on March 11th; please see SAT’s website for topics covered by the workshop and submission guidelines.</p>

<p>Bendert Zevenbergen <a href="https://lists.torproject.org/pipermail/tor-talk/2015-February/036939.html" rel="nofollow">relayed</a> another call for participation, this time in the ACM SigComm2015 workshop on “Ethics in Networked Systems Research”.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

