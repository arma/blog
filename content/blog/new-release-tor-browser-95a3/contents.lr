title: New Release: Tor Browser 9.5a3
---
pub_date: 2019-12-04
---
author: boklm
---
tags:

tor browser
tbb
tbb-9.5
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a3 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a3/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-902">latest stable release</a> instead.</p>
<p>This release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2019-37/">security updates</a> to Firefox.</p>
<p>This new alpha release picks up security fixes for Firefox 68.3.0esr and updates our external extensions (NoScript and HTTPS Everywhere) to their latest versions. Among other things, we made some cleanups in torbutton and fixed localization in the Android bundles. We also add three new locales: lt (Lithuanian), ms (Maylay), and th (Thai). Please help us test those new locales if you speak those languages.</p>
<h3>Reproducible Builds</h3>
<p>The issue with reproducible builds mentioned in the <a href="https://blog.torproject.org/new-release-tor-browser-901">9.0.1 blog post</a> is still present in this release. However, we made progress on <a href="https://trac.torproject.org/projects/tor/ticket/32053">understanding the issue</a> and are getting closer to a fix.</p>
<h3>ChangeLog</h3>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt">changelog</a> since Tor Browser 9.5a2 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.3.0esr</li>
<li>Bump NoScript to 11.0.9
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32362">Bug 32362</a>: NoScript TRUSTED setting doesn't work</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32429">Bug 32429</a>: Issues with about:blank and NoScript on .onion sites</li>
</ul>
</li>
<li>Update HTTPS Everywhere to 2019.11.7</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32618">Bug 32618</a>: Backport fixes from Mozilla bugs 1467970 and 1590526</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32606">Bug 32606</a>: Set up default bridge at Georgetown University</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30787">Bug 30787</a>: Add lt locale</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30788">Bug 30788</a>: Add ms locale</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30786">Bug 30786</a>: Add th locale</li>
<li>Translations update</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/28746">Bug 28746</a>: Remove torbutton isolation and fp prefs sync</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/28745">Bug 28745</a>: Assume always running in Tor Browser</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30888">Bug 30888</a>: move torbutton_util.js to modules/utils.js</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30851">Bug 30851</a>: Move default preferences to 000-tor-browser.js</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/28745">Bug 28745</a>: Remove torbutton.js unused code</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32255">Bug 32255</a>: Missing ORIGIN header breaks CORS</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Tor to 0.4.2.4-rc</li>
<li>Update Tor Launcher to 0.2.20.3
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30787">Bug 30787</a>: Add lt locale</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30788">Bug 30788</a>: Add ms locale</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30786">Bug 30786</a>: Add th locale</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/30237">Bug 30237</a>: Improve TBB UI of hidden service client authorization</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32365">Bug 32365</a>: Localization is broken in Tor Browser 9 on Android</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32405">Bug 32405</a>: Crash immediately after bootstrap on Android</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32505">Bug 32505</a>: Tighten our rules in our entitlements file for macOS</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/32616">Bug 32616</a>: Disable GetSecureOutputDirectoryPath() functionality</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-285864"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285864" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2019</p>
    </div>
    <a href="#comment-285864">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285864" class="permalink" rel="bookmark">Bug 30787: Add lt locale
   …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>    Bug 30787: Add lt locale<br />
    Bug 30787: Add th locale
</p></blockquote>
<p>something went wrong...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285865"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285865" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">December 05, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285864" class="permalink" rel="bookmark">Bug 30787: Add lt locale
   …</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285865">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285865" class="permalink" rel="bookmark">Thanks, fixed.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, fixed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285879"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285879" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jr (not verified)</span> said:</p>
      <p class="date-time">December 05, 2019</p>
    </div>
    <a href="#comment-285879">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285879" class="permalink" rel="bookmark">just updated Tor Browser and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>just updated Tor Browser and virus detected in load.<br />
idp.elexa.51<br />
is this an issue?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285885"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285885" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2019</p>
    </div>
    <a href="#comment-285885">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285885" class="permalink" rel="bookmark">https://trac.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://trac.torproject.org/projects/tor/ticket/32536" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/32536</a> is still an issue.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Johnny (not verified)</span> said:</p>
      <p class="date-time">December 07, 2019</p>
    </div>
    <a href="#comment-285916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285916" class="permalink" rel="bookmark">I think I have a very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think I have a very interesting point to make... currently tor circuit chooses a fixed first server "guard" that doesn't change often (because of some sort of attack if I recall correctly).</p>
<p>I would like to see a option to allow also a: "FIXED GUARD EXIT" (probably adding one additional hop server in the connection loop will be need to have some additional protection).</p>
<p>Why? Many, many web sites simply don't work, even if they don't try to block Onion ("Tor") because they use sometimes multiple URL's and require captchas (ex.: download web sites) and they associate the correct resolution of the captchas to the IP, but since the onion ("Tor") creates a different IP for each URL makes it impossible to use many web sites. Also many web sites don't have captchas but if one logs in and the IP changes during the session they will log out the user for security reasons... in Tor Browser these can happen a lot!</p>
<p>Because always exiting on the same IP may help identify someone it should be some special group tab session or something like that, so that the user absolutely knows it is using a fixed IP at the exit point, and that should have a permanent warning informing that, informing that it should only be used on web sites that don't work properly in the normal "Tor Browser" session. The special group tab should allow opening of new windows using the same exit point IP address since I've notice web sites doing that on the past where they will open a new URL in new windows/ tabs with the thing that the user wants after successfully solving captchas/ seeing the publicity... but the new URL will NEVER work because the IP's don't match... and the same to see images where one can't grab them because the servers see different IP's (they have one URL to display and other to host the images) and don't allow the download.</p>
<p>I hope you can add these to help make Tor Browser more useful.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286036"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286036" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285916" class="permalink" rel="bookmark">I think I have a very…</a> by <span>Johnny (not verified)</span></p>
    <a href="#comment-286036">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286036" class="permalink" rel="bookmark">U need chrome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>U need chrome</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285917"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285917" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Notimpressed (not verified)</span> said:</p>
      <p class="date-time">December 07, 2019</p>
    </div>
    <a href="#comment-285917">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285917" class="permalink" rel="bookmark">I can no longer disable the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can no longer disable the Tor Network and use my own VPN.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285933"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285933" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Munch48 (not verified)</span> said:</p>
      <p class="date-time">December 08, 2019</p>
    </div>
    <a href="#comment-285933">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285933" class="permalink" rel="bookmark">I found that the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I found that the conformation dont come through after you sign up ?<br />
Is this a problem or is it just delayed ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285935"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285935" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">December 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285933" class="permalink" rel="bookmark">I found that the…</a> by <span>Munch48 (not verified)</span></p>
    <a href="#comment-285935">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285935" class="permalink" rel="bookmark">After you sign up to what?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After you sign up to what?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285941"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285941" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2019</p>
    </div>
    <a href="#comment-285941">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285941" class="permalink" rel="bookmark">Noscript blue popup to allow…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Noscript blue pop-up to allow media applies permanent custom per-site permissions in noscript options. These permissions persist after new identity and closing browser. I expected new identity or closing browser to revert blue pop-up. I understand that noscript options applied manually are supposed to persist, but I was not expecting blue permissions to persist because the pop-up is exposed to users whether or not noscript icon is hidden.  Are blue pop-up permissions intended to persist?</p>
<p>Permissions can be reverted manually in noscript options or by changing security level.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285954"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285954" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Axe (not verified)</span> said:</p>
      <p class="date-time">December 10, 2019</p>
    </div>
    <a href="#comment-285954">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285954" class="permalink" rel="bookmark">Letterboxing is disabled?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Letterboxing is disabled?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285974"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285974" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">December 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285954" class="permalink" rel="bookmark">Letterboxing is disabled?</a> by <span>Axe (not verified)</span></p>
    <a href="#comment-285974">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285974" class="permalink" rel="bookmark">Letterboxing is still…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Letterboxing is still enabled by default.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285971"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285971" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2019</p>
    </div>
    <a href="#comment-285971">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285971" class="permalink" rel="bookmark">TypeError: controller is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TypeError: controller is null textbox.js:230:7<br />
    doCommand chrome://global/content/elements/textbox.js:230<br />
    _initUI chrome://global/content/elements/textbox.js:110</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285976"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285976" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2019</p>
    </div>
    <a href="#comment-285976">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285976" class="permalink" rel="bookmark">TBB doesn&#039;t allow media from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TBB doesn't allow media from 3rd party if it's not allowed for a 1st party.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285984"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285984" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2019</p>
    </div>
    <a href="#comment-285984">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285984" class="permalink" rel="bookmark">svg.disabled</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Switching svg.disabled to false breaks NoScript's per-site settings and change everything from Safest to Safer level! That's not what I want!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2019</p>
    </div>
    <a href="#comment-286018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286018" class="permalink" rel="bookmark">I&#039;m pressing &#039;New Circuit…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm pressing 'New Circuit for this Site' continuously, and the guard changes randomly between the two relays: one in Germany and one in France. What's going on?</p>
</div>
  </div>
</article>
<!-- Comment END -->
