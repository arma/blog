title: Join us for the State of the Onion 2023
---
pub_date: 2023-11-26
---
author: pavel
---
categories:

community
fundraising

---
summary: We will be hosting our annual State of the Onion livestream, a virtual two-day event featuring the Tor Project's different teams presenting highlights of their work from 2023 and what we are excited about in the upcoming year, on November 29th and December 6th from 17:00 - 18:00 UTC.
---
body:

It's that time of the year again! The Tor Project is inviting you to our annual virtual two-day event where we share updates from the organization and community–highlighting their work, this year's accomplishments and demonstrating all the different use cases for Tor applications and implementations. 

Like last year, we are organizing two streams one week apart. So please make sure to save the date for both events!

- **Wednesday, November 29, 17:00 - 18:00 UTC => [State of the Onion with the presenters from the Tor Project](https://www.youtube.com/watch?v=hdFL0kXu440).**
- **Wednesday December 6, 17:00 - 18:00 UTC => [State of the Onion featuring presenters from the Tor Community](https://www.youtube.com/watch?v=TtgTwho1wfQ).**

The State of the Onion will be live streamed on our [YouTube](https://www.youtube.com/c/TorProjectInc) account. Join the conversation on social media using the hashtag: #StateOfTheOnion2023 or post your questions and comments in the YouTube chat.

## Nov 29 Program - State of the Onion: The Tor Project’s teams.

From thwarting [DDoS attacks](https://blog.torproject.org/tor-network-ddos-attack/) to expanding our anti-censorship toolkit amid ongoing censorship challenges around the world, the Tor Project has introduced many infrastructure improvements from its [POW-defense for .onion services](https://blog.torproject.org/introducing-proof-of-work-defense-for-onion-services/) to Webtunnel, Conjure and many more. On top of bolstering the Tor network's performance, our two major browser releases continue to advance Tor Browser's [usability](https://blog.torproject.org/new-release-tor-browser-125/) and [accessibility](https://blog.torproject.org/new-release-tor-browser-130/). Plus, we'll talk about how releasing [Mullvad Browser](https://blog.torproject.org/releasing-mullvad-browser/) in collaboration with Mullvad VPN has allowed us to reach more people and provided us with many learnings from a broader range of users.

On Day 1 of our State of the Onion event the applications, anti-censorship, and community teams as well as many others will talk about the impact their work in 2023 has had on [defending people's right to privacy and anonymity everywhere](https://blog.torproject.org/empowering-human-rights-defenders/). Isabela Fernandes, the Tor Project's executive director, will be your host for the event.

## Dec 6 Program - State of the Onion for the Tor community.

Day 2 will put a spotlight on some of our community members and like-minded organizations. Presenters from the [Guardian Project](https://guardianproject.info/), [OONI](https://ooni.org/), [Tails](https://tails.net/), [Quiet](https://tryquiet.org/) will demonstrate all the different ways to implement Tor technology, and how they have made an impact on people's lives around the world.

We'll also hear from [Nos Oignons](https://nos-oignons.net/), a [relay association](https://community.torproject.org/relay/community-resources/relay-associations/) that is celebrating 10 years in 2023, and the Electronic Frontier Foundation (EFF), who will recap this year's [EFF Tor University Challenge](https://toruniversity.eff.org/), to learn more about the adventures of running Tor relays.

Roger Dingledine, Co-Founder/President of the Tor Project, will be this event's host.

__________________________________________________

This event is part of our [year-end fundraising campaign](https://blog.torproject.org/2023-fundraiser-make-a-donation-to-Tor/). You can support the Tor Project's work by [making a donation](https://donate.torproject.org/) today. We couldn't do the work we're sharing at this year's State of the Onion without your support!


[![Donate Button](yec-button.png "class=align-center")](https://torproject.org/donate/donate-bp4-2023)

You can check out our previous State of the Onion streams [2021](https://www.youtube.com/watch?v=mNhIjtXuVzk) and [2022](https://youtube.com/live/uSyBZ7GIzJY?feature=share) and [2022 Community Day](https://youtube.com/live/O-7k0PjnBbk?feature=share) on our YouTube channel.

