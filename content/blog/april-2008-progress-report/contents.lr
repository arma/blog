title: April 2008 Progress Report
---
pub_date: 2008-05-14
---
author: phobos
---
tags:

progress report
torbutton
tor browser
release candidate
translation
---
categories:

applications
localization
releases
reports
---
_html_body:

<p>Tor 0.2.0.24-rc (released Apr 22) adds dizum (run by Alex de Joode)<br />
as the new sixth v3 directory authority, makes relays with dynamic IP<br />
addresses and no DirPort notice more quickly when their IP address<br />
changes, fixes a few rare crashes and memory leaks, and fixes a few<br />
other miscellaneous bugs. Tor 0.2.0.25-rc (released Apr 23) makes Tor<br />
work again on OS X and certain BSDs.<br />
<a href="http://archives.seul.org/or/talk/May-2008/msg00014.html" rel="nofollow">http://archives.seul.org/or/talk/May-2008/msg00014.html</a></p>

<p>Torbutton 1.1.18 (released Apr 17) fixes many usability and interoperability<br />
items, in an attempt to make the new Torbutton not so obnoxious in its<br />
zeal to protect the user. It also includes new translations for French,<br />
Russian, Farsi, Italian, and Spanish.</p>

<p>We did a complete overhaul of the <a href="https://check.torproject.org/" rel="nofollow">https://check.torproject.org/</a><br />
page. Now it accepts a language choice,<br />
e.g. <a href="https://check.torproject.org/?lang=fa-IR" rel="nofollow">https://check.torproject.org/?lang=fa-IR</a><br />
Available languages are German, English, Spanish, Italian, Farsi,<br />
Japanese, Polish, Portugese, Russian, and Chinese. The Tor Browser<br />
Bundle automatically uses the appropriate language as its home page,<br />
based on which language of the Browser Bundle was downloaded.</p>

<p>Started on a documentation page to explain to users what bridges are,<br />
how they can decide whether they need one, and how to configure their<br />
Tor client to use them:<br />
<a href="https://www.torproject.org/bridges.html" rel="nofollow">https://www.torproject.org/bridges.html</a></p>

<p>We've also started working on a design proposal for making it easier<br />
to set up a private or testing Tor network. With the advent of the v3<br />
directory protocol, it currently takes up to 30 minutes before a test<br />
network will produce a useful networkstatus consensus. Also, there are<br />
a dozen different config options that need to be set correctly for<br />
a Tor network running on a single IP address to not trigger various<br />
security defenses. This approach should let more people set up their<br />
own Tor networks, either for testing or because they can't reach the<br />
main Tor network.<br />
<a href="https://www.torproject.org/svn/trunk/doc/spec/proposals/135-private-tor-networks.txt" rel="nofollow">https://www.torproject.org/svn/trunk/doc/spec/proposals/135-private-tor…</a></p>

<p>We have the beginnings of a grand plan for how to successfully scale<br />
the Tor network to orders of magnitude more relays than we have<br />
currently. Much more work and thinking remain.<br />
<a href="https://www.torproject.org/svn/trunk/doc/spec/proposals/ideas/xxx-grand-scaling-plan.txt" rel="nofollow">https://www.torproject.org/svn/trunk/doc/spec/proposals/ideas/xxx-grand…</a></p>

<p>We also did a retrospective on currently open but not finished design<br />
proposals, so we don't have as many "open" proposals in the pipeline<br />
but not getting attention:<br />
<a href="http://archives.seul.org/or/dev/Apr-2008/msg00009.html" rel="nofollow">http://archives.seul.org/or/dev/Apr-2008/msg00009.html</a></p>

<p>We added several more research papers that we'd like to see written to<br />
the <a href="https://www.torproject.org/volunteer#Research" rel="nofollow">https://www.torproject.org/volunteer#Research</a> page. In May we'll add<br />
a few more and then start pointing academic professors at the new list.</p>

<p>The development version of Vidalia now has GUI boxes to configure an http<br />
proxy that Vidalia should launch when it starts. (The Tor Browser Bundle<br />
already uses these config options internally to launch Polipo when it<br />
starts.) The next steps are to make sure that Polipo (our preferred new<br />
http proxy) is stable enough on Windows, and then start shipping some<br />
new standard bundles with Polipo rather than Privoxy.</p>

<p>We cleaned up the Torbutton install in the OS X bundles so it installs<br />
Torbutton for the local user, rather than global. Hopefully this will<br />
make OS X users happier.</p>

<p>We're making progress on integrating a UPnP library into Vidalia. This<br />
feature will allow users who want to set up a Tor relay but don't want<br />
to muck with manual port forwarding on their router/firewall to just<br />
click a button and have Vidalia interact with their router/firewall<br />
automatically. This approach won't work in all cases, but it should work<br />
in at least some. We hope to land the first version of this in May.</p>

<p>Steven Murdoch and Robert Watson worked towards a final version of<br />
their PETS 2008 paper called "Metrics for Security and Performance in<br />
Low-Latency Anonymity Systems." The final version will be available in<br />
May at:<br />
<a href="http://www.cl.cam.ac.uk/~sjm217/papers/pets08metrics.pdf" rel="nofollow">http://www.cl.cam.ac.uk/~sjm217/papers/pets08metrics.pdf</a></p>

<p>So far there appear to be no free-software zip splitters that work<br />
on Windows and produce self-contained exe files for automatically<br />
reconstructing the file. Rather than using a closed-source shareware<br />
application (as it seems a shame to put a trust gap in our build process<br />
when we don't need to), the current plan is to write some instructions<br />
for users to fetch the 7zip program, and then fetch a set of blocks,<br />
and run a batch file to reconstruct them. We're in the process of trying<br />
to learn how large the blocks can be -- preliminary guess is 2MB.</p>

<p>We have a first draft of a translation portal up here:<br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>The Vidalia GUI now has (manual) translation instructions:<br />
<a href="http://trac.vidalia-project.net/wiki/Translations" rel="nofollow">http://trac.vidalia-project.net/wiki/Translations</a></p>

<p>We've registered the Vidalia project on "LaunchPad", which is a<br />
web-based translation site that is compatible with Vidalia's string<br />
format:<br />
<a href="https://translations.launchpad.net/vidalia/trunk/+pots/vidalia" rel="nofollow">https://translations.launchpad.net/vidalia/trunk/+pots/vidalia</a><br />
We're currently working to try to upload our current translations into<br />
the LaunchPad interface.</p>

<p>We've registered the Torbutton project on "BabelZilla", which is a<br />
web-based translation site designed specifically for Firefox extensions.<br />
We've uploaded the current translation strings:<br />
<a href="http://www.babelzilla.org/index.php?option=com_wts&amp;Itemid=88&amp;extension=3510&amp;type=lang" rel="nofollow">http://www.babelzilla.org/index.php?option=com_wts&amp;Itemid=88&amp;extension=…</a></p>

<p>Lastly, we've begun developer-oriented documentation for how to manage<br />
and maintain these various translation web-interfaces:<br />
<a href="https://www.torproject.org/svn/trunk/doc/translations.txt" rel="nofollow">https://www.torproject.org/svn/trunk/doc/translations.txt</a></p>

---
_comments:

<a id="comment-107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.batterylaptoppower.com">battery (not verified)</a> said:</p>
      <p class="date-time">June 27, 2008</p>
    </div>
    <a href="#comment-107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-107" class="permalink" rel="bookmark">RE: The Bug Squad Meets Unit Testing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[…]Good site I “Stumbledupon” it today and gave it a stumble for you.. looking forward to seeing what else you have..later[…]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1096"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1096" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Hayden Panettiere (not verified)</span> said:</p>
      <p class="date-time">May 06, 2009</p>
    </div>
    <a href="#comment-1096">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1096" class="permalink" rel="bookmark">the current plan is to write</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the current plan is to write some instructions<br />
for users to fetch the 7zip program, and then fetch a set of blocks,<br />
and run a batch file to reconstruct them.<br />
<a href="http://www.mustuniversity.com/Schools-Majors/Law-and-Legal-Studies.html" rel="nofollow">online law degree programs</a> | <a href="http://www.mustuniversity.com/Schools-Majors/Engineering.html" rel="nofollow">online engineering degree programs</a> | <a href="http://www.mustuniversity.com/Schools-Majors/Business-and-Management.html" rel="nofollow">online business administration degrees</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1925"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1925" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Brian (not verified)</span> said:</p>
      <p class="date-time">July 24, 2009</p>
    </div>
    <a href="#comment-1925">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1925" class="permalink" rel="bookmark">So far there appear to be no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So far there appear to be no free-software zip splitters that work<br />
on Windows and produce self-contained exe files for automatically<br />
reconstructing the file. <a href="http://waterproof-digital-cameras.org/" rel="nofollow">water proof camera review</a> and <a href="http://waterproof-digital-cameras.org/olympus-waterproof-digital-camera/" rel="nofollow">top rated waterproof cameras</a> and <a href="http://waterproof-digital-cameras.org/olympus-waterproof-digital-cameras/" rel="nofollow">olymbus waterproof</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2910"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2910" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 22, 2009</p>
    </div>
    <a href="#comment-2910">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2910" class="permalink" rel="bookmark">Torbutton 1.1.18 (released</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Torbutton 1.1.18 (released Apr 17) fixes many usability and interoperability<br />
items, in an attempt to make the new Torbutton not so obnoxious in its<br />
zeal to protect the user. It also includes new translations for French,<br />
Russian, Farsi, Italian, and Spanish.<a href="http://www.onlineflashgames.org" rel="nofollow">online games</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
