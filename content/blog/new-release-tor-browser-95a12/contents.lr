title: New Release: Tor Browser 9.5a12
---
pub_date: 2020-05-07
---
author: sysrqb
---
tags:

tbb
tbb-9.5
tor browser
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a12 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a12/">distribution directory.</a></p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-9010">latest stable release</a> instead.</p>
<p>This release updates Firefox to 68.8.0esr, NoScript to 11.0.25, OpenSSL to 1.1.1g, and Tor to 0.4.3.4-rc. In addition, the Android Tor Browser now includes Tor built using the reproducible build system.</p>
<p>Also, this release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-17/">security updates</a> to Firefox.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">changelog</a> since Tor Browser 9.5a11 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.8.0esr</li>
<li>Bump NoScript to 11.0.25</li>
<li>Bump Tor to 0.4.3.4-rc</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/31499">Bug 31499:</a> Update libevent to 2.1.11-stable
<ul>
<li><a href="https://bugs.torproject.org/33877">Bug 33877</a>: Disable Samples and Regression tests For Libevent Build</li>
</ul>
</li>
<li><a href="https://bugs.torproject.org/33630">Bug 33630</a>: Remove noisebridge01 default bridge</li>
<li><a href="https://bugs.torproject.org/33726">Bug 33726</a>: Fix patch for #23247: Communicating security expectations for .onion</li>
<li><a href="https://bugs.torproject.org/34017">Bug 34017:</a> Bump openssl version to 1.1.1g</li>
</ul>
</li>
<li> Windows + OS X + Linux
<ul>
<li>Update Tor Launcher to 0.2.21.6
<ul>
<li>Translations update</li>
</ul>
</li>
<li><a href="https://bugs.torproject.org/33576">Bug 33576</a>: Update pion-webrtc version to 2.2.3</li>
<li><a href="https://bugs.torproject.org/32418">Bug 32418</a>: Allow updates to be disabled via an enterprise policy.</li>
<li><a href="https://bugs.torproject.org/34032">Bug 34032</a>: Use Securedrop's Official https-everywhere ruleset</li>
<li><a href="https://bugs.torproject.org/33698">Bug 33698</a>: Update "About Tor Browser" links in Tor Browser</li>
</ul>
</li>
<li> Windows
<ul>
<li><a href="https://bugs.torproject.org/29614">Bug 29614</a>: Use SHA-256 algorithm for Windows timestamping</li>
</ul>
</li>
<li> Android
<ul>
<li><a href="https://bugs.torproject.org/33359">Bug 33359</a>: Use latest Version of TOPL and Remove Patches</li>
<li><a href="https://bugs.torproject.org/33931">Bug 33931</a>: obfs4 bridges are used instead of meek if meek is selected in Tor Browser for Android alpha</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li><a href="https://bugs.torproject.org/32027">Bug 32027</a>: Bump Go to 1.13.10</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/28765">Bug 28765</a>: LibEvent Build for Android</li>
<li><a href="https://bugs.torproject.org/28766">Bug 28766</a>: Tor Build for Android</li>
<li><a href="https://bugs.torproject.org/32993">Bug 32993</a>: Package Tor With Tor Android Service Project</li>
<li><a href="https://bugs.torproject.org/33685">Bug 33685</a>: Add Support for Building zlib for Android</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://bugs.torproject.org/33802">Bug 33802</a>: --enable-secure-api is not supported anymore in mingw-w64</li>
</ul>
</li>
</ul>
</li>
</ul>

