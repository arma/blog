title: TorBirdy 0.2.1 is released
---
pub_date: 2016-12-01
---
author: sukhbir
---
tags:

TorBirdy
Thunderbird
icedove
email
---
_html_body:

<p>We are pleased to announce the seventh beta release of TorBirdy: TorBirdy 0.2.1. </p>

<p>This release fixes an annoying usability issue where TorBirdy sets the calendar timezone to UTC thus overriding the local timezone and breaking the calendar functionality; see  commit <a href="https://gitweb.torproject.org/torbirdy.git/commit/?id=3ea8e5d3280515d1ac15f1e7002bce05fff29ba7" rel="nofollow">3ea8e5d</a> and Bug <a href="https://bugs.torproject.org/20157" rel="nofollow">20157</a> for more information.</p>

<p>If you are using TorBirdy for the first time, visit the <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">wiki</a> to get started.</p>

<p>There are currently no known leaks in TorBirdy but please note that we are still in beta, so the usual caveats apply.</p>

<p>Here is the complete changelog since v0.2.0 (released on 23 June 2016):</p>

<blockquote><p>
    0.2.1, 30 Nov 2016<br />
     * Bug <a href="https://bugs.torproject.org/20157" rel="nofollow">20157</a>: Do not set calendar timezone to UTC<br />
     *  Bug <a href="https://bugs.torproject.org/20750" rel="nofollow">20750</a>, <a href="https://bugs.torproject.org/20644" rel="nofollow">20644</a>: Ensure RSS feeds are displayed in plain text<br />
     * Revert setting no_proxies_on to an empty string (see commit <a href="https://gitweb.torproject.org/torbirdy.git/commit/?id=b2f6a45bdfebe71072bd9c791a92383ecc087dc2" rel="nofollow">b2f6a45b</a>)<br />
     * Added support for automatic configuration of systemli.org email accounts
</p></blockquote>

<p>We offer two ways of installing TorBirdy: by visiting our <a href="https://dist.torproject.org/torbirdy/torbirdy-current.xpi" rel="nofollow">website</a> (<a href="https://dist.torproject.org/torbirdy/torbirdy-current.xpi.asc" rel="nofollow">GPG signature</a>; signed by [geshifilter-code]&lt;a href="<a href="https://www.torproject.org/docs/signing-keys.html.en&quot;&gt;0xB01C8B006DA77FAA&lt;/a&gt;[/geshifilter-code" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en"&gt;0xB01C8B006DA77FA…</a>]) or by visiting the <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/" rel="nofollow">Mozilla Add-ons page</a> for TorBirdy. Please note that there may be a delay -- which can range from a few hours to days -- before the extension is reviewed by Mozilla and updated on the Add-ons page. </p>

<p>(<a href="https://packages.debian.org/sid/xul-ext-torbirdy" rel="nofollow">Packages</a> for Debian GNU/Linux will be created and uploaded shortly by Ulrike Uhlig.)</p>

---
_comments:

<a id="comment-223578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223578" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223578" class="permalink" rel="bookmark">I&#039;m very happy to read this!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm very happy to read this! </p>
<p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223588"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223588" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223588">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223588" class="permalink" rel="bookmark">Thanks :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223660"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223660" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223660">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223660" class="permalink" rel="bookmark">Does it really protect the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does it really protect the login/authentication passwords of my Thunderbird emailboxes? Or is there  a chance someone else will catch them somewhere along the line?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223670"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223670" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223660" class="permalink" rel="bookmark">Does it really protect the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223670">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223670" class="permalink" rel="bookmark">If your provider uses</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If your provider uses TLS/SSL (which every provider pretty much does these days) then your messages to and from your email server are encrypted and thus no node in the Tor route can read their contents.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223670" class="permalink" rel="bookmark">If your provider uses</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223691" class="permalink" rel="bookmark">Does this addon *enforce*</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does this addon *enforce* the use of SSL while connecting to those mailboxes?<br />
I shudder at the thought of getting my email through Tor with no encryption (even if just by accident).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223691" class="permalink" rel="bookmark">Does this addon *enforce*</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223802" class="permalink" rel="bookmark">yes, it enforces tls v 1.1</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>yes, it enforces tls v 1.1</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223702"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223702" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223670" class="permalink" rel="bookmark">If your provider uses</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223702">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223702" class="permalink" rel="bookmark">Please make an equivalent of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please make an equivalent of the "HTTPS Only" Firefox addon.<br />
So TorBirdy won't be vulnerable to SSL downgrade attacks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223800"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223800" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223702" class="permalink" rel="bookmark">Please make an equivalent of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223800">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223800" class="permalink" rel="bookmark">You can open Torbirdy and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can open Torbirdy and set a tick at {{{Transparent Torificion}}}, than all the torbidy seetings are applied but without using the tor deamon on your pc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223820"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223820" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223800" class="permalink" rel="bookmark">You can open Torbirdy and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223820">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223820" class="permalink" rel="bookmark">I feel this is not a good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I feel this is not a good solution as it creates a new problem of sending your traffic over the clearnet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223801"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223801" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223702" class="permalink" rel="bookmark">Please make an equivalent of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223801">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223801" class="permalink" rel="bookmark">Of course you could just set</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Of course you could just set them by making some changes to the about:counfig, too. (there are more seeting that might be intressting like not sending your LAN-ip, the email agent string etc)</p>
<p>but to enforce the stronger cipher and certificate pinning all you have to do is to go to the thunderird prefrences open the `advanced editor`</p>
<p>and set</p>
<p><i><br />
security.ssl3.* false    // the asterisk stands for <b>all</b> entries<br />
security.ssl3.ecdhe_rsa_aes_128_gcm_sha256 true<br />
security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256 true</i></p>
<p>prevent insecure recognition<br />
security.ssl.require_safe_negotiation true<br />
security.ssl.treat_unsafe_negotiation_as_broken true</p>
<p>security.cert_pinning.enforcement_level 2<br />
</p>
<p>there's a great page describing literally all the necessary setp unfortunately it's just in German (but hey, feel free to use a translator tool to get the gist)</p>
<p><a href="https://privacy-handbuch.de/handbuch_31d.htm" rel="nofollow">https://privacy-handbuch.de/handbuch_31d.htm</a><br />
<a href="https://privacy-handbuch.de/handbuch_31k.htm" rel="nofollow">https://privacy-handbuch.de/handbuch_31k.htm</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223818"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223818" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223801" class="permalink" rel="bookmark">Of course you could just set</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223818">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223818" class="permalink" rel="bookmark">Thanks for the comment. For</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the comment. For reference this is also being discussed in <a href="https://trac.torproject.org/projects/tor/ticket/20751" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/20751</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223702" class="permalink" rel="bookmark">Please make an equivalent of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223805" class="permalink" rel="bookmark">Have a look at the torbirdy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have a look at the torbirdy wiki, please!</p>
<p><a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/torbirdy</a><br />
<a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/torbirdy</a></p>
<p><i>    Connection security for both incoming and outgoing servers is set to SSL/TLS. </i></p>
<p>But don't forget you are using Tor, ie you a free to <b>use Tor-Services</b> (formely known as <b>hiddenservices</b>) and generally speaking use pof onion addresses at least mitigates some of the risks of using ssl/tls-certificates.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223819"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223819" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223805" class="permalink" rel="bookmark">Have a look at the torbirdy</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223819">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223819" class="permalink" rel="bookmark">Just to add to this comment,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just to add to this comment, we try to enforce TLS for existing as well as new accounts. But yes, if your mail provider has an onion service, you should use that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-223850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
    <a href="#comment-223850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223850" class="permalink" rel="bookmark">Thanks! Do you think</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! Do you think TorBirdy will ever be in Tails?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223857"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223857" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223850" class="permalink" rel="bookmark">Thanks! Do you think</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223857">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223857" class="permalink" rel="bookmark">TorBirdy is already in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TorBirdy is already in Tails: <a href="https://tails.boum.org/doc/anonymous_internet/icedove/index.en.html" rel="nofollow">https://tails.boum.org/doc/anonymous_internet/icedove/index.en.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223878"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223878" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to sukhbir</p>
    <a href="#comment-223878">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223878" class="permalink" rel="bookmark">I saw that when I read the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I saw that when I read the next blog post "Tor at the Heart: Torbirdy" Shows how much I know! I never found it when using Tails so I just assumed it wasn't included because of its beta status or something else. The debian branding of Thunderbird also confused me a little bit at first. Thanks for the link!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-223876"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223876" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
    <a href="#comment-223876">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223876" class="permalink" rel="bookmark">Torbirdy changes the local</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Torbirdy changes the local IP that's part of the header to 127.0.0.1 ("fully qualified domain name"), wouldn't it be better to set an IP address that's more likely to be there in the first place? Like 192.168.0.x ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223880"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223880" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sukhbir
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sukhbir said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223876" class="permalink" rel="bookmark">Torbirdy changes the local</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223880">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223880" class="permalink" rel="bookmark">See</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=812115" rel="nofollow">https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=812115</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-232548"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232548" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
    <a href="#comment-232548">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232548" class="permalink" rel="bookmark">Is there a work around for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a work around for getting TorBirdy to work with the latest alpha series of Tor browser? It fails to establish a connection.</p>
</div>
  </div>
</article>
<!-- Comment END -->
