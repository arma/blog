title: New Alpha Release: Tor Browser 13.0a4 (Android, Windows, macOS, Linux)
---
pub_date: 2023-09-14
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0a4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0a4 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a4/).

This release updates Firefox to 115.2.1esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-40/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-34/) from Firefox 117.

## Major Changes

This is our fourth alpha release in the 13.0 series which represents a transition from Firefox 102-esr to Firefox 115-esr. This builds on a year's worth of upstream Firefox changes, so alpha-testers should expect to run into issues. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.org/c/feedback/tor-browser-alpha-feedback/6).

We are in the middle of our annual esr transition audit, where we review Mozilla's year's worth of work with an eye for privacy and security issues that would negatively affect Tor Browser users. This will be completed before we transition the 13.0 alpha series to stable. At-risk users should remain on the 102-esr based 12.5 stable series which will continue to receive security updates until 13.0 alpha is promoted to stable.

### Build Output Naming Updates

As a reminder from the [13.0a3](https://blog.torproject.org/new-alpha-release-tor-browser-130a3) release post, we have made the naming scheme for all of our build outputs mutually consistent. If you are a downstream packager or in some other way download Tor Browser artifacts in scripts or automation, you will have a bit more work to do beyond bumping the version number once the 13.0 alpha stabilizes. All of our current build outputs can be found in the [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a4/)

### UX Refresh of about:tor

The about:tor page you land on after bootstrapping has been rewritten for our Desktop platforms. As part of this process, and as part of the tor integration back-end rewrite, we have removed the automatic tor network connectivity check ( https://check.torproject.org ) which occurred in the about:tor page.

This check was a hold-over from the tor-launcher days when launching and bootstrapping the tor daemon was handled by an extension which ran *before* the Firefox browser interface was presented to the user. As a result of the tighter tor integration and in-browser bootstrapping experience in about:connection, the legacy logic behind this check would sometimes fail and present *some* users with the infamous ['red screen of death'](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/f32328), even if their tor connection was fine.

That is to say, all of the reports we have received of users hitting this screen were false-positives when using the default configuration. The conditions for which the check on this page made sense no longer exist and now only serve to confuse users. On top of that, the two main environments where Tor Browser is used in a non-default configuration where the check is arguably useful ([Tails](https://tails.net) and Whonix) do not use the built-in about:tor page for home or new-tab.

Tor Browser users with the default configuration who successfully go through the bootstrapping process essentially cannot get into a situation where they are able to load about:tor while not being connected to the process-owned tor daemon. If they are connected to the tor daemon, then the check will either succeed or timeout if the connection to the Tor Network fails after bootstrapping. If the tor daemon has crashed or failed to launch, then the browser's proxy settings prevent web traffic from going anywhere outside the users system

In the short term, we will be adding some [ux to the about:tor page](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/32328#note_2935444) for users who are *not* using a default configuration to easily check that their configuration is correct and using tor as expected.

Longer-term (in the 13.5 time-frame) we plan on integrating this tor check directly into the about:connection [state-machine](https://gitlab.torproject.org/tpo/applications/tor-browser/-/blob/tor-browser-115.2.1esr-13.0-1/browser/modules/TorConnect.sys.mjs?ref_type=heads#L63) so we can avoid false-positives in the default configuration while also providing peace-of-mind that web traffic is being routed correctly. We will also likely iterate on the about:tor ux for users in non-default configurations.

### Android

Our Tor Browser Android release should be pretty close to final in terms of changes, apart from bug fixes or tweaks required by our annual ESR code-audit. The rendering+branding errors from 13.0a3 have been resolved. If you are able, please be sure to take the Tor Browser Android alpha for a spin, and especially try using bridges!

## Known Issues

### Desktop

Build to build incremental updates are currently failing for some users if you are starting at a version older than 13.0a3. Users on 13.0a2 and 13.0a1 will first download the small incremental update, fail to apply it after a re-launch, and then download the full large update. This should not result in losing anything of value apart from your precious time.

It is being tracked in [tor-browser#42101](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42101).

### Windows

Building generated debug headers are not currently reproducible. This only affects debug info and does not affect users. This issue is being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41995). It will either be fixed before the 13.0 alpha series transitions to stable later this year, or we will disable this developer feature by default to ensure fully matching builds.

## Full changelog

We would like to thank volunteer contributor cypherpunks1 for their fixes for [tor-browser#41876](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41876) and [tor-browser#41740](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41740).

The full changelog since [Tor Browser 13.0a3](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.27
  - Updated tor to 0.4.8.5
  - Updated Snowflake to 2.6.1
  - [Bug tor-browser-build#40951](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40951): Firefox fails to build for macos after #40938
  - [Bug tor-browser#41675](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41675): ESR115: decide on large array buffers
  - [Bug tor-browser#41740](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41740): ESR115: change devicePixelRatio spoof to 2 in alpha for testing
  - [Bug tor-browser#41797](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41797): Lock RFP in release builds
  - [Bug tor-browser#41934](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41934): Websocket raises DOMException on http onions in 13.0a1
  - [Bug tor-browser#42034](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42034): aboutTBUpdate.dtd is duplicated
  - [Bug tor-browser#42043](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42043): Disable gUM: media.devices.enumerate.legacy.enabled
  - [Bug tor-browser#42061](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42061): Move the alpha update channel creation to a commit on its own
  - [Bug tor-browser#42084](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42084): Race condition with language preferences may make spoof_english ineffective
  - [Bug tor-browser#42093](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42093): Rebase alpha onto 115.2.1esr
- Windows + macOS + Linux
  - Updated Firefox to 115.2.1esr
  - [Bug tor-browser-build#40149](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40149): Remove patching of nightly update URL
  - [Bug tor-browser-build#40821](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40821): The update details URL is wrong in alphas
  - [Bug tor-browser-build#40938](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40938): Copy the new tor-browser.ftl file to the appropriate directory
  - [Bug tor-browser#41333](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41333): Modernize Tor Browser's new-tab page (about:tor)
  - [Bug tor-browser#41528](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41528): Hard-coded English "based on Mozilla Firefox" appears in version in "About" dialog
  - [Bug tor-browser#41651](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41651): Use moz-toggle in connection preferences
  - [Bug tor-browser#41739](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41739): Remove "Website appearance"
  - [Bug tor-browser#41774](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41774): Hide the new "Switching to a new device" help menu item
  - [Bug tor-browser#41821](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41821): Fix the proxy type in the proxy modal of about:preferences in 13.0
  - [Bug tor-browser#41865](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41865): Use --text-color-deemphasized rather than --panel-description-color
  - [Bug tor-browser#41876](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41876): Remove firefox view from title bar
  - [Bug tor-browser#41881](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41881): Developer tools/Network/New Request remembers requests
  - [Bug tor-browser#41886](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41886): Downloads drop-down panel has new-line/line-break between every word in the 'Be careful opening downloads' warning
  - [Bug tor-browser#41904](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41904): The log textarea doesn't resize anymore
  - BUg 41906: hide about:preferences#privacy &gt; DNS over HTTPS section [tor-browser]
  - [Bug tor-browser#41971](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41971): Update Tails URL in downloads warning
  - [Bug tor-browser#41974](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41974): De-emphasized text in custom components is no longer gray in 13.0 alpha
  - [Bug tor-browser#41977](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41977): Hide the "Learn more" link in bridge cards
  - [Bug tor-browser#41980](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41980): Circuit display headline is misaligned in 13.0 alpha
  - [Bug tor-browser#42027](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42027): Create a Base Browser version of migrateUI
  - [Bug tor-browser#42045](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42045): Circuit panel overflows with long ipv6 addresses
  - [Bug tor-browser#42046](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42046): Remove XUL layout hacks from base browser
  - [Bug tor-browser#42047](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42047): Remove layout hacks from tor browser preferences
  - [Bug tor-browser#42050](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42050): Bring back Save As... dialog as default
  - [Bug tor-browser#42075](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42075): Fix link spacing and underline on new homepage
- Windows + Android
  - Updated zlib to 1.3
  - [Bug tor-browser-build#40930](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40930): Update zlib to 1.3 after 13.0a3
- macOS
  - [Bug tor-browser#42057](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42057): Disable Platform text-recognition functionality
- Linux
  - [Bug tor-browser#41509](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41509): After update, KDE Plasma identifies Tor Browser Nightly window group as "firefox-nightly"
- Android
  - Updated GeckoView to 115.2.1esr
  - [Bug tor-browser-build#40941](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40941): Remove PT process options on Android
  - [Bug tor-browser#41878](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41878): firefox-mobile: refactor tor bootstrap off deleted onboarding path
  - [Bug tor-browser#41879](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41879): firefox-android: Add Tor integration and UI commit is too big, needs to be split up
  - [Bug tor-browser#41882](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41882): Update DuckDuckGo icons
  - [Bug tor-browser#41987](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41987): Tor Browser Android Onboarding Plan
  - [Bug tor-browser#42001](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42001): Hide 'Open links in external app' settings option and force defaults
  - [Bug tor-browser#42038](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42038): TBA Alpha - inscriptions Tor Browser Alpha and FireFox Browser simultaneously on the start screen
  - [Bug tor-browser#42076](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42076): Theme is visable in options, but shouldn't be
- Build System
  - All Platforms
    - Updated Go to 1.21.1
    - [Bug tor-browser-build#40929](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40929): Update go to 1.21 series after 13.0a3
    - [Bug tor-browser-build#40932](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40932): Remove appname_bundle_android, appname_bundle_macos, appname_bundle_linux, appname_bundle_win32, appname_bundle_win64 from projects/release/update_responses_config.yml
    - [Bug tor-browser-build#40935](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40935): Fix fallout from build target rename in signing scripts
    - [Bug tor-browser-build#40948](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40948): Remove lyrebird-vendor sha256sum in nightly
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40786](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40786): deploy_update_responses-*.sh requires +r permissions to run
    - [Bug tor-browser-build#40931](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40931): Fix incrementals after tor-browser-build#40829
    - [Bug tor-browser-build#40933](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40933): Fix generating incrementals between 12.5.x and 13.0
    - [Bug tor-browser-build#40942](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40942): Use the branch to build Base Browser
    - [Bug tor-browser-build#40944](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40944): After #40931, updates_responses is using incremental.mar files as if they were non-incremental mar files
    - [Bug tor-browser-build#40946](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40946): override_updater_url does not work for Mullvad Browser
    - [Bug tor-browser-build#40947](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40947): Remove migrate_langs from tools/signing/nightly/update-responses-base-config.yml
  - macOS
    - [Bug tor-browser-build#40943](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40943): Update libdmg-hfsplus to include our uplifted patch
    - [Bug tor-browser#42035](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42035): Update tools/torbrowser/ scripts to support macOS dev environment
  - Linux
    - [Bug tor-browser#42071](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42071): tor-browser deployed format changed breaking fetch.sh
