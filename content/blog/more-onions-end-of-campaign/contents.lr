title: End of #MoreOnionsPorFavor campaign
---
pub_date: 2020-08-25
---
author: ggus
---
tags: onion services
---
categories: onion services
---
_html_body:

<p><em>This week we're officially wrapping up the campaign <a href="https://blog.torproject.org/more-onions-porfavor">#MoreOnionsPorFavor</a>. Non-profits, companies, media outlets, whistleblower platforms, service providers, hackerspaces, security conferences, bloggers, and web developers joined the campaign to make the web more secure. We will email swag winners in the next few days.</em></p>
<div class="ace-line" id="magicdomid6"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">Over the last month, onion services operators and our broad community celebrated and deployed a brand new feature called <a href="https://support.torproject.org/onionservices/onion-location/">Onion-Location</a>. The feature, a purple pill </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">in</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> the URL bar, advertises to users that there’s a more secure way to connect </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">to a</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> site</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z"> by</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> using onion services. Even though onion service saw the light of the day <a href="https://blog.torproject.org/v2-deprecation-timeline">more than 15 years ago</a></span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">, #MoreOnionsPorFavor was our first outreach campaign to run onion sites, and more specifically to deploy Onion-Location.</span></div>
<div class="ace-line" id="magicdomid7"> </div>
<div class="ace-line" id="magicdomid8"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">As part of the Tor Project</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">'s</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> mission, we're a non-profit with the goal to educate users about anonymity and privacy technologies that we build. During the campaign, we've learned that although many people know about the vanilla onion routing design, where a user connects over three relays to reach a website, many technologists aren't aware of how onion services actually work or what </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">is</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> needed to onionize a website.</span></div>
<div class="ace-line" id="magicdomid9"> </div>
<div class="ace-line" id="magicdomid10"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">That is why </span><span class="author-a-gtyz81zz80z0z87znz75zuz66zyatz73zz69z">the</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> campaign and </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">the </span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">Onion-Location feature are both important: </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">together, they educate users that onion services are just a secure way to access the internet, like HTTPS but without metadata. The campaign was also a great opportunity for websites to advertise or remind users that they have an onion site. That was the case </span><span class="author-a-gtyz81zz80z0z87znz75zuz66zyatz73zz69z">for</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z"> <a href="https://fragdenstaat.de">FragDenStaat.de</a>, a non-profit organisation from Germany:</span></div>
<div class="ace-line" id="magicdomid11"> </div>
<blockquote><div class="ace-line" id="magicdomid12"><em><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">We have been serving Tor browser traffic on our domain via our </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">hidden</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> service</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z"> [onion service]</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> for a while through use of the Alt-Svc HTTP header, but your blog post gave us the incentive to make our onion service more visible.</span></em></div>
</blockquote>
<div class="ace-line" id="magicdomid13"> </div>
<div class="ace-line" id="magicdomid14"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">In addition, for many onion services operators, the possibility </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">of</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> hav</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">ing</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> their online services reachable worldwide and bypass</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">ing</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> local censorship is an important motivation for the adoption of onion services technology. This is the specific case of media outlets that are looking to ensure and promote </span><span class="author-a-gtyz81zz80z0z87znz75zuz66zyatz73zz69z">press </span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">freedom. Joining our campaign, <em>Deutsche Welle</em> <a href="https://www.dwnewsvdyyiamwnp.onion/en/dw-websites-accessible-via-tor-protocol/a-51338328">wrote</a>:</span></div>
<div class="ace-line" id="magicdomid15"> </div>
<blockquote><div class="ace-line" id="magicdomid17"><em><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">DW is a global advocate for freedom of opinion and freedom of speech. […] It is therefore a logical step for us to also use Tor to reach people in censored markets who previously had limited or no access to free media.</span></em></div>
</blockquote>
<div class="ace-line" id="magicdomid18"> </div>
<div class="ace-line" id="magicdomid28"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">Some of us are passionate about onions and </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">their</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> security properties. </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">Over 60 organisations and individuals -- s</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">mall, medium</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">,</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> and large onions</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z"> -- </span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">ha</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">ve</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> reached out to us to be part of this campaign. Setting up an onion site can be important not only for us and our users, but also to teach your colleagues and community about how important is to protect client data, like this french criminal lawyer, who shared their onion site with us:</span></div>
<div class="ace-line" id="magicdomid30"><span class="author-a-9rf26l2z76zz88zyyz71z27z78z1">    </span></div>
<blockquote><div class="ace-line" id="magicdomid22"><em><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">I would like to let you know that I, a french criminal lawyer, use a hidden service </span></em><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">[onion service]</span><em><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> on my professional website. […] I use it to promote Tor to other lawyers who often use encrypted message services but do not consider the matter of anonymity online, even though some famous French criminal lawyers have recently been subject to highly questionable judicial interceptions. The legal battle is not exclusive to the use of technical precautionary measures such as Tor...</span></em></div>
</blockquote>
<div class="ace-line" id="magicdomid24"> </div>
<div class="ace-line"><img alt="Onion sites" src="/static/images/blog/inline-images/onions-2020.png" class="align-center" /></div>
<div class="ace-line" id="magicdomid25"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">In the </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">coming</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> months</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">,</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> we’re going to improve the documentation of onion services section on the Community portal with the feedback collected </span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">through this campaign </span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">and with tools that we developed to help to p</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">romote</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> onion sites. As we mentioned previously, we offered technical support for enterprise</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z">s</span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z"> that wanted to join this campaign. If you’re reading about the campaign now and want to set</span><span class="author-a-z84zz81zz86zuvokq0ez83zdz65zt2z71z"> </span><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">up an onion site for your organisation, send an email to us: <a href="mailto:frontdesk@torproject.org">frontdesk@torproject.org</a>. </span></div>
<div class="ace-line" id="magicdomid26"> </div>
<div class="ace-line" id="magicdomid27"><span class="author-a-z88zstjz66zpz89zq1z89zz79z4z85zqsz83z">Finally, we want to send a big <strong>thank you</strong> to all the participants and a special thank you to our friend @<a href="https://twitter.com/cyberdees">cyberdees</a>, who is an unstoppable Tor advocate!</span></div>

---
_comments:

<a id="comment-289257"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289257" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">August 26, 2020</p>
    </div>
    <a href="#comment-289257">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289257" class="permalink" rel="bookmark">Onion services with DV/SOOC…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Onion services with DV/SOOC X.509 certs when? It's time to get rid of the EV certificate snake oil!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289275"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289275" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2020</p>
    </div>
    <a href="#comment-289275">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289275" class="permalink" rel="bookmark">Shout out to Deutsche Welle,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shout out to Deutsche Welle, which offers English language translations of important stories about US/RU cyberwar activities targeting DE which have not been covered in the US press! :-)</p>
<p>&gt; In the coming months, we’re going to improve the documentation of onion services section on the Community portal with the feedback collected through this campaign and with tools that we developed to help to promote onion sites. As we mentioned previously, we offered technical support for enterprises that wanted to join this campaign</p>
<p>So no more tech support for onions?  That would be too bad.</p>
<p>The news about the DDOS attacks on onions is unsettling and I still don't like the sound of the proposed fix using tokens (possibly because I don't understand the technical details), but I am glad that TP is not giving up on the very notion of onions!</p>
<p>One very important onion service you did not mention in the post, which IMO is in particular need of attention, are the onion mirrors of the Debian FOSS software repositories.  Please work with Debian Project to maintain awareness of how bad the DDOS problem is for those onions specifically.  Recall that one of the revelations from the Snowden leaks is that NSA (and  presumably the "services" of other nations) freely abuses insecurities in software downloads to inject malware into user computers, quite indiscriminately even when their immediate goal is to pop a "small" list of targeted users.  Note that the Snowden leak documents (and the JA analysis published by DW about a year after the first Snowden leaks were published), the "small" lists can actually include entire large classes of users, such as </p>
<p>o employees of ISPs, banks, energy sector, etc, from any country<br />
o climate scientists and diplomats from a currently targeted country<br />
o employees of local government agencies such as police or unemployment from any country<br />
o reporters from any country, e.g. DW, TheGuardian, Washington Post,<br />
o employees of irritant NGOs such as Wikileaks.org, irritant bloggers,<br />
o anti-nuclear protesters, protesters against unpopular US military bases (e.g. Okinawa)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289298"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289298" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289275" class="permalink" rel="bookmark">Shout out to Deutsche Welle,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289298">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289298" class="permalink" rel="bookmark">Hello, 
We asked some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, </p>
<p>We asked some Debian developers about enabling Onion-Location and they told us that it will only happen when they update OnionBalance to a newer version on their repositories. And that's why Debian onions aren't mentioned in the post.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289286"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289286" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John Smith (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>
    <a href="#comment-289286">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289286" class="permalink" rel="bookmark">What about small websites…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about small websites like blogs and stuff? They should also get coverage!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289299"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289299" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289286" class="permalink" rel="bookmark">What about small websites…</a> by <span>John Smith (not verified)</span></p>
    <a href="#comment-289299">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289299" class="permalink" rel="bookmark">Hello John,
We had the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello John,<br />
We had the participation of bloggers and individuals. They got social media coverage and some of them will get a Tor swag. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289297"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289297" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>
    <a href="#comment-289297">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289297" class="permalink" rel="bookmark">I wish and would love if…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wish and would love if Wikipedia had an onion. Do you remember that one time someone set up an unofficial onion of Wikipedia for fun/experiment?  Do you remember how insanely popular it was before he shut it down?  Yeah.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289303"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289303" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 28, 2020</p>
    </div>
    <a href="#comment-289303">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289303" class="permalink" rel="bookmark">Why isn&#039;t Mozilla&#039;s domain…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why isn't Mozilla's domain on an onion service? It seems perfectly natural...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289438"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289438" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 12, 2020</p>
    </div>
    <a href="#comment-289438">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289438" class="permalink" rel="bookmark">It would be good to turn the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It would be good to turn the Onion-Location spec proposal into an IETF Internet-Draft, and eventually become an RFC. Even if it's only an intermediate step, if it were more widely adopted it would in effect reduce load on exit nodes, no?</p>
</div>
  </div>
</article>
<!-- Comment END -->
