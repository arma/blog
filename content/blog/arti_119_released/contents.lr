title: Arti 1.1.9 is released: Assembling the onions
---
author: nickm
---
pub_date: 2023-10-03
---
categories: announcements
---
summary:

Arti 1.1.9 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.9.

Arti 1.1.9 continues work on support for onion services in arti.
We now have the code needed to publish onion service descriptors;
keep them up-to-date with changes 
and our introduction points;
receive, decrypt, process, and answer introduction requests;
and respond to them by delivering traffic to local ports.
The pieces are now (mostly) connected; the next month of development
will see extensive testing, bugfixing, and refinement.

For full details, and for information about
many smaller and less visible changes as well;
for those, please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Emil Engler and Saksham Mittal!


Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-119-2-october-2023
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/
