title: New Release: Tails 5.14
---
pub_date: 2023-06-13
---
author: tails
---
categories:

partners
releases
---
summary:

It's a major version with a new backup feature in Tails Installer, automatic upgrade of the Persistent Storage to LUKS2, and captive portal detection.
---
body:

# New features

## Automatic migration to LUKS2 and Argon2id

The cryptographic parameters of LUKS from Tails 5.12 or earlier are weak
against a state-sponsored attacker with physical access to your device.

To use stronger encryption parameters, Tails 5.14 automatically converts your
Persistent Storage to use LUKS2 encryption with Argon2id.

Still, we recommend you change the passphrase of your Persistent Storage and
other LUKS encrypted volumes unless you use a long passphrase of 5 random
words or more.

[Read our security advisory and upgrade
guide.](https://tails.net/security/argon2id/)

## Full backups from Tails Installer

You can now do a backup of your Persistent Storage from _Tails Installer_ by
cloning your Persistent Storage to your backup Tails entirely.

You can still use the backup utility to go faster while [updating your
backup](https://tails.net/doc/persistent_storage/backup/#updating).

## Captive portal detection

Tails now detects if you have to sign in to the network using a captive portal
if you choose to connect to Tor automatically.

The error screen appears more quickly and recommends you try to sign in to the
network as the first option.

## Incentive to donate from _Electrum_

Many people use Tails to secure their Bitcoin wallet and donations in Bitcoin
are key to the survival of our project, so we integrated a way to donate from
_Electrum_ in Tails.

# Changes and updates

## Included software

  * Update _Tor Browser_ to [12.0.7](https://blog.torproject.org/new-release-tor-browser-1207).

## Usability improvements to the Persistent Storage

  * Change the button to create a Persistent Storage from the Welcome Screen to be a switch. ([#19673](https://gitlab.tails.boum.org/tails/tails/-/issues/19673))

  * Add back the description of some of the Persistent Storage features and mention _Kleopatra_ in the _GnuPG_ feature. ([#19642](https://gitlab.tails.boum.org/tails/tails/-/issues/19642) and [#19675](https://gitlab.tails.boum.org/tails/tails/-/issues/19675))

  * Hide the duplicated _Persistent_ bookmark in the _Files_ browser. ([#19646](https://gitlab.tails.boum.org/tails/tails/-/issues/19646))

# Fixed problems

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

  * Avoid restarting the desktop environment when creating a Persistent Storage. ([#19667](https://gitlab.tails.boum.org/tails/tails/-/issues/19667))

# Known issues

None specific to this release.

See the list of [long-standing
issues](https://tails.net/support/known_issues/).

# Get Tails 5.14

## To upgrade your Tails USB stick and keep your persistent storage

  * Automatic upgrades are available from Tails 5.0 or later to 5.14.

You can [reduce the size of the
download](https://tails.net/doc/upgrade/#reduce) of future automatic upgrades
by doing a manual upgrade to the latest version.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a [manual upgrade](https://tails.net/doc/upgrade/#manual).

## To install Tails on a new USB stick

Follow our installation instructions:

  * [Install from Windows](https://tails.net/install/windows/)
  * [Install from macOS](https://tails.net/install/mac/)
  * [Install from Linux](https://tails.net/install/linux/)
  * [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.net/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

## To download only

If you don't need installation or upgrade instructions, you can download Tails
5.14 directly:

  * [For USB sticks (USB image)](https://tails.net/install/download/)
  * [For DVDs and virtual machines (ISO image)](https://tails.net/install/download-iso/)

# Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.
