title: Tor 0.2.2.22-alpha is out
---
pub_date: 2011-01-27
---
author: erinn
---
tags:

tor
bridges
alpha release
bridge blocking
---
categories:

circumvention
network
releases
---
_html_body:

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>Changes in version 0.2.2.22-alpha - 2011-01-25<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a bounds-checking error that could allow an attacker to<br />
      remotely crash a directory authority. Bugfix on 0.2.1.5-alpha.<br />
      Found by "piebeer".</li>
<li>Don't assert when changing from bridge to relay or vice versa<br />
      via the controller. The assert happened because we didn't properly<br />
      initialize our keys in this case. Bugfix on 0.2.2.18-alpha; fixes<br />
      bug 2433. Reported by bastik.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Adjust our TLS Diffie-Hellman parameters to match those used by<br />
      Apache's mod_ssl.</li>
<li>Provide a log message stating which geoip file we're parsing<br />
      instead of just stating that we're parsing the geoip file.<br />
      Implements ticket 2432.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Check for and reject overly long directory certificates and<br />
      directory tokens before they have a chance to hit any assertions.<br />
      Bugfix on 0.2.1.28 / 0.2.2.20-alpha. Found by "doorss".</li>
</ul>

