title: Volunteer Spotlight: Sina Rabbani Helps Activists Avoid Government Censorship
---
pub_date: 2018-08-14
---
author: tommy
---
tags: volunteer spotlight
---
summary: We’re grateful to have the support of a dedicated volunteer base who help us to make Tor the strongest privacy tool out there, and we’re highlighting their work in this series. We want to thank Sina Rabbani, one of the co-founders (and former CTO) of Access Now, a nonprofit dedicated to defending users’ digital rights, for his years of support to Tor and to the internet freedom movement.
---
_html_body:

<p> </p>
<p>Tor is a labor of love built by a small group of committed individuals. We’re grateful to have the support of a dedicated volunteer base who help us to make Tor the strongest privacy tool out there, and we’re highlighting their work in this series. We want to thank Sina Rabbani, one of the co-founders (and former CTO) of <a href="https://accessnow.org/">Access Now</a>, a nonprofit dedicated to defending users’ digital rights, for his years of support to Tor and to the internet freedom movement.</p>
<p>Sina runs<a href="https://metrics.torproject.org/rs.html#details/CF6D0AAFB385BE71B8E111FC5CFF4B47923733BC"> Faravahar</a>, one of nine Tor directory authorities. These <a href="https://blog.torproject.org/introducing-bastet-our-new-directory-authority">dedicated servers</a> tell the millions of Tor clients which relays make up the Tor network. A talented and passionate engineer, Sina has been involved with digital rights activism for almost a decade. Today, he’s a Systems Engineer with Team Cymru, an internet security company which analyzes threat intelligence.</p>
<p>In 2009, he was one of the four co-founders of Access Now. For a time, he also served as the organization’s Chief Technology Officer.</p>
<p>That same year, Sina started to see videos and images of people being shot and killed as they protested peacefully in the streets of Tehran. In response, he used his considerable energy and knowledge to set up network proxies and software that would download videos with Farsi keywords from YouTube and reupload them. By mirroring the content, he made it harder for Iranian authorities to censor images and video from what many dubbed “the Twitter revolution.”</p>
<p>Soon after, he discovered Tor, met co-founder and president Roger Dingledine, and, in his own words, “fell in love with the project immediately.” He began to <a href="https://blog.torproject.org/new-guide-running-tor-relay">run relays</a> and, long before our campaign to <a href="https://blog.torproject.org/aggregation-feed-types/onionize-web">onionize the web</a>, also helped activists to create onion service versions of their websites, ensuring that the sites were accessible even when governments tried to block them or take them offline.</p>
<p><img alt="pic-sina-rabbani" src="/static/images/blog/inline-images/sina-tor-volunteer.jpeg" class="align-center" /></p>
<p>For Sina, free speech is something that he doesn’t take for granted. “I was born in a country where you can be sentenced to death because of your speech,” he tells us. “Freedom of speech in the digital age is a basic human right. Tyranny will start by taking our ability to speak up first, then the rest of our rights.”</p>
<p>“Tor gives me a chance to resist tyranny in a non-violent manner, and I feel blessed and grateful for the opportunity. The hope is to one day move Faravahar to one of Iran’s universities. Until that day, <em>Ma Hastim va Ma Bishomarim</em> — we are, and we are countless.”</p>
<p>We’re grateful for Sina’s tireless work making the internet safer and more secure for millions of users. With his help, Tor is fighting against those who want to make censorship the norm and privacy a thing of the past.</p>
<h2>Join Our Community</h2>
<p>Getting involved with Tor is easy. <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide">Run a relay</a> to make the network faster and more decentralized. Learn about each of our teams and start collaborating.</p>
<p>Tor is a vital tool for protecting privacy and resisting repressive censorship and surveillance. If you want to make a contribution but don’t have the time to volunteer, <a href="https://donate.torproject.org">your donation</a> helps keep Tor fast, strong, and secure.</p>

