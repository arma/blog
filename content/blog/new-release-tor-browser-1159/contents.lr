title: New Release: Tor Browser 11.5.9 (Android)
---
pub_date: 2022-11-23
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.9 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.9/).

This is an Android-only release which updates the Android Target API level to 31 to meet Google Play requirements (see: https://support.google.com/googleplay/android-developer/answer/11926878)

The full changelog since [Tor Browser 11.5.8](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-11.5/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt) is:

- Android
 - [Bug tor-browser#41471](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41471): Update targetSdkVersion to 31
