title: July 2008 Progress Report
---
pub_date: 2008-08-18
---
author: phobos
---
tags:

tor
vidalia
progress report
bridges
torbutton
tor browser
translation
proposals
research
---
categories:

applications
circumvention
localization
network
reports
research
---
_html_body:

<p><strong>Releases:</strong></p>

<p>Torbutton 1.2.0rc5 (released July 6) provides improved addon compatibility, better preservation of Firefox preferences that we touch, fixing issues with Tor toggle breaking for some option combos, and an improved 'Restore Defaults' button. This version also features Firefox 3 cookie jar support, and support for storing cookie jars in memory.<br />
<a href="http://archives.seul.org/or/talk/Jul-2008/msg00026.html" rel="nofollow">http://archives.seul.org/or/talk/Jul-2008/msg00026.html</a></p>

<p>Vidalia 0.1.6 (released July 8) fixes a bug introduced in 0.1.3 that could cause excessive CPU usage or crashing on some platforms; continues to prepare Vidalia's strings for easier translation; adds a Romanian GUI and installer translation; and updated the Farsi, Finnish, French, German, and Swedish translations.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.6/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.6/CHAN…</a></p>

<p>Tor 0.2.0.29-rc (released July 8) fixes two big bugs with using bridges, fixes more hidden-service performance bugs, and fixes a bunch of smaller bugs.<br />
<a href="http://archives.seul.org/or/talk/Jul-2008/msg00038.html" rel="nofollow">http://archives.seul.org/or/talk/Jul-2008/msg00038.html</a></p>

<p>Torbutton 1.2.0rc6 (released July 12) features fixes for a nasty history loss bug, an exception during Tor toggle, javascript being disabled in some tabs, better pref handling, and more.<br />
<a href="http://archives.seul.org/or/talk/Jul-2008/msg00049.html" rel="nofollow">http://archives.seul.org/or/talk/Jul-2008/msg00049.html</a></p>

<p>Tor 0.2.0.30 (released July 15) is the first stable release of the 0.2.0.x branch. The previous stable branch (0.1.2.x) went stable in April of 2007. We are still waiting for Torbutton and Vidalia to stabilize before announcing the Windows and OS X packages on the or-announce announcements<br />
list. We expect to do that in August.</p>

<p>Tor Browser Bundle 1.1.1 (released July 20) updates Vidalia to release 0.1.6, updates Pidgin Portable to 2.4.3, updates Pidgin OTR plugin to 3.2, updates Tor to 0.2.1.2-alpha, updates Torbutton to 1.2.0rc6, and sets TZ=UTC environment variable in RelativeLink (needed by Torbutton).<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/README" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/README</a></p>

<p>Torbutton 1.2.0 (released July 30) is finally a stable release for the new Torbutton tree that includes application-level privacy protections.<br />
<a href="https://svn.torproject.org/svn/torbutton/trunk/src/CHANGELOG" rel="nofollow">https://svn.torproject.org/svn/torbutton/trunk/src/CHANGELOG</a></p>

<p>From the Tor 0.2.0.29-rc ChangeLog:<br />
"When a hidden service was trying to establish an introduction point, and Tor had built circuits preemptively for such purposes, we were ignoring all the preemptive circuits and launching a new one instead. Bugfix on 0.2.0.14-alpha."<br />
"When a hidden service was trying to establish an introduction point, and Tor *did* manage to reuse one of the preemptively built circuits, it didn't correctly remember which one it used, so it asked for another one soon after, until there were no more preemptive circuits, at which point it launched one from scratch. Bugfix on 0.0.9.x."</p>

<p>The upcoming Tor 0.2.1.3-alpha and 0.2.1.4-alpha releases include more fixes for hidden service performance and robustness, have slightly improved bootstrap status event behavior, and start hunting down a horrible bug that looks like it could leak private information:<br />
<a href="https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=779" rel="nofollow">https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=779</a></p>

<p><strong>Proposals:</strong></p>

<p>Proposal 145 (Separate "suitable as a guard" from "suitable as a new guard") suggests one approach for separating the role of "is still useful as an entry guard" from "should be an option when choosing a new entry guard". This step will help us load balance over the network better.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/145-newguard-flag.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/145-newguar…</a></p>

<p>Proposal 146 (Add new flag to reflect long-term stability) discusses how to ship the Tor client with a set of alternate sources for initial bootstrap directory information. We already have this feature in Tor 0.2.0.x, called the "fallback consensus", but we never enabled it because the Tor client would spend too long trying directory mirrors that were long since gone from the network. This proposal moves us closer to being able to distinguish the more long-term reliable mirrors.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/146-long-term-stability.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/146-long-te…</a></p>

<p>Proposal 147 (Eliminate the need for v2 directories in generating v3 directories) helps wean us off of needing the old deprecated v2 directory design. Currently we only use it to give advance warning to the v3 authorities about relays that haven't heard about yet, so they can fetch information about those relays before the time arrives to make an official vote about their state.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/147-prevoting-opinions.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/147-prevoti…</a></p>

<p>Proposal 148 (Stream end reasons from the client side should be uniform) describes a simple fix for a potential anonymity flaw in Tor's core protocol for passing explanations from one end of a Tor circuit to the other when an application stream ends.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/148-uniform-client-end-reason.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/148-uniform…</a></p>

<p>Proposal 149 (Using data from NETINFO cells) starts talking about how to make use of the timestamp and IP address listed in Tor's new NETINFO cells. In theory we can use them to decide if our clock is skewed, and to decide if a traffic analysis man-in-the-middle attack is happening against us. In practice it appears more complex than we expected.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/149-using-netinfo-data.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/149-using-n…</a></p>

<p>Proposal 150 (Exclude Exit Nodes from a circuit) allows users to specify which relays should never be used as the last (exit) hop in a circuit. We took the proposal one step further and allowed users to also specify IP addresses and netmasks for which relays to avoid in the exit position.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/150-exclude-exit-nodes.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/150-exclude…</a></p>

<p>Proposal 151 (Improving Tor Path Selection) is a draft proposal to implement the results of Fallon Chen's Google Summer of Code project. Her plan is to measure the expected time it takes to establish a circuit, and then abandon circuits that take significantly longer than that to form. The assumption is that circuits that take a long time to set up will generally have unacceptably high latency as well.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/151-path-selection-improvements.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/151-path-se…</a></p>

<p>Proposal 154 (Automatic Software Update Protocol) starts the discussion of how to let Vidalia automatically manage updates for Tor, Polipo, Vidalia, etc. This is very important for keeping users up to date with respect to security and stability fixes. We will especially aim to do the updates over Tor, a) for privacy, and b) so users who are blocked from the Tor website will still be able to upgrade seamlessly.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/154-automatic-updates.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/154-automat…</a></p>

<p><strong>Research:</strong></p>

<p>Karsten Loesing's report on 7 ways to improve the performance and robustness of Tor hidden services:<br />
<a href="http://freehaven.net/~karsten/hidserv/discussion-2008-07-15.pdf" rel="nofollow">http://freehaven.net/~karsten/hidserv/discussion-2008-07-15.pdf</a></p>

<p>Four new research papers on Tor came out in July:<br />
<a href="http://freehaven.net/anonbib/#loesing2008performance" rel="nofollow">http://freehaven.net/anonbib/#loesing2008performance</a><br />
<a href="http://freehaven.net/anonbib/#improved-clockskew" rel="nofollow">http://freehaven.net/anonbib/#improved-clockskew</a><br />
<a href="http://freehaven.net/anonbib/#mccoy-pet2008" rel="nofollow">http://freehaven.net/anonbib/#mccoy-pet2008</a><br />
<a href="http://freehaven.net/anonbib/#danezis-pet2008" rel="nofollow">http://freehaven.net/anonbib/#danezis-pet2008</a></p>

<p>We continued evaluating the TBB footprints here:<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/docs/traces.txt" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/docs/traces.txt</a></p>

<p>In particular, we added a new "Registry modifications" section to that file, describing some new traces that appear to be left behind after operating Tor Browser Bundle, even from the USB key. One of the most worrying is the "user assist" registry key that gets set, and (incredible as it sounds) is obfuscated by rot-13 before being set.</p>

<p><strong>Ease of Use:</strong></p>

<p>Tor Browser Bundle 1.1.1 (released July 20) updates Vidalia to release 0.1.6, updates Pidgin Portable to 2.4.3, updates Pidgin OTR plugin to 3.2, updates Tor to 0.2.1.2-alpha, updates Torbutton to 1.2.0rc6, and sets TZ=UTC environment variable in RelativeLink (needed by Torbutton).</p>

<p>The first Incognito (Gentoo-based Tor LiveCD) release of 2008 is also nearing completion, and we expect to see it released in August.</p>

<p>Finally, we contracted to start work on the Tor VM project. The idea is to run a Linux kernel and a Tor client inside a thin VM (like QEMU) on Windows, and then transparently intercept outgoing connections and redirect them into Tor. This approach will a) make proxy-avoiding side-channel and sidejacking attacks less devastating, and b) isolate the Tor client from the rest of the OS to provide a more robust security approach. Current design document is under development at<br />
<a href="https://svn.torproject.org/svn/torvm/trunk/doc/design.html" rel="nofollow">https://svn.torproject.org/svn/torvm/trunk/doc/design.html</a></p>

<p><strong>Getting Tor:</strong></p>

<p>We have established our "gettor" email auto-responder script that lets people mail <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a> and retrieve a copy of Tor from their mailbox. We still need to ponder more usability issues, such as translation.<br />
<a href="https://www.torproject.org/finding-tor" rel="nofollow">https://www.torproject.org/finding-tor</a></p>

<p>We have also automated the process of checking Tor website mirrors: there's a new update-mirrors.pl script in the website directory that generates a list of mirrors ordered by when they last synced with the main website.<br />
<a href="https://www.torproject.org/mirrors" rel="nofollow">https://www.torproject.org/mirrors</a></p>

<p><strong>Translations:</strong></p>

<p>We have our translation server up and online:<br />
<a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a></p>

<p>We revised our translation tutorial here:<br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>Users continued to submit updated translations for many different languages.</p>

<p>We continued enhancements to the Chinese and Russian Tor website<br />
translations. We added Vidalia, Torbutton, and website translations<br />
into Farsi.</p>

<p>We also added the strings for Vidalia's installer; this required writing several scripts to convert from the "nsh" (nullscript installer language) format to the "po" (preferred by Pootle) format and back.</p>

