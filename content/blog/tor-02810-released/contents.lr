title: Tor 0.2.8.10 is released
---
pub_date: 2016-12-02
---
author: nickm
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>There's a new stable version of Tor! </p>

<p>Tor 0.2.8.10 backports a fix for a bug that would sometimes make clients unusable after they left standby mode. It also backports fixes for a few portability issues and a small but problematic memory leak.</p>

<p>You can download the source from the usual place on the website. Packages should be available over the next several days, including a TorBrowser release around December 14. Remember to check the signatures!</p>

<p>Below are the changes since 0.2.8.9.</p>

<h2>Changes in version 0.2.8.10 - 2016-12-02</h2>

<ul>
<li>Major bugfixes (client reliability, backport from 0.2.9.5-alpha):
<ul>
<li>When Tor leaves standby because of a new application request, open circuits as needed to serve that request. Previously, we would potentially wait a very long time. Fixes part of bug <a href="https://bugs.torproject.org/19969" rel="nofollow">19969</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (client performance, backport from 0.2.9.5-alpha):
<ul>
<li>Clients now respond to new application stream requests immediately when they arrive, rather than waiting up to one second before starting to handle them. Fixes part of bug <a href="https://bugs.torproject.org/19969" rel="nofollow">19969</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability, backport from 0.2.9.6-rc):
<ul>
<li>Work around a bug in the OSX 10.12 SDK that would prevent us from successfully targeting earlier versions of OSX. Resolves ticket <a href="https://bugs.torproject.org/20235" rel="nofollow">20235</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability, backport from 0.2.9.5-alpha):
<ul>
<li>Fix implicit conversion warnings under OpenSSL 1.1. Fixes bug <a href="https://bugs.torproject.org/20551" rel="nofollow">20551</a>; bugfix on 0.2.1.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (relay, backport from 0.2.9.5-alpha):
<ul>
<li>Work around a memory leak in OpenSSL 1.1 when encoding public keys. Fixes bug <a href="https://bugs.torproject.org/20553" rel="nofollow">20553</a>; bugfix on 0.0.2pre8.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 3 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-223817"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223817" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
    <a href="#comment-223817">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223817" class="permalink" rel="bookmark">Lots of references to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Lots of references to OpenSSL 1.1 in this changelog, and I think I remember that TorBrowser v6.0.x is packaged with OpenSSL v1.0.1 (why not v1.02?).</p>
<p>What is the preferred version of OpenSSL for use with Tor 0.2.8.x?  With Tor v0.2.9.x?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223824"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223824" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223817" class="permalink" rel="bookmark">Lots of references to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223824">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223824" class="permalink" rel="bookmark">I&#039;d suggest 1.0.2 with both.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'd suggest 1.0.2 with both. 1.0.1 should work fine too, but it will be unsupported after 31 Dec 2016.  OpenSSL 1.1 should work better with 0.2.9.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224025"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224025" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223817" class="permalink" rel="bookmark">Lots of references to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224025">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224025" class="permalink" rel="bookmark">1.0.1 is still fine and was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1.0.1 is still fine and was until now as well. We are shipping 1.0.2 in the alpha series to test whether there are any issues. So far none showed up and the plan is to turn the current alpha series into the next major stable in January: Tor Browser 6.5 won't have 1.0.1 anymore.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223830"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223830" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
    <a href="#comment-223830">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223830" class="permalink" rel="bookmark">Tor 0.2.8.10 backports a fix</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><cite>Tor 0.2.8.10 backports a fix for a bug that would sometimes make clients unusable after they left standby mode.</cite></p>
<p>Ah! So that's why I wouldn't be able to connect in the Tor Browser after I put my notebook on standby only after closing and then re-opening the Tor Browser? Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223832" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223830" class="permalink" rel="bookmark">Tor 0.2.8.10 backports a fix</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223832" class="permalink" rel="bookmark">Yes! This one drove me nuts.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes! This one drove me nuts. We used to be great at recognizing that the network had come back, and then in 0.2.8.1-alpha we broke that.</p>
<p>See<br />
<a href="http://bugs.torproject.org/19969" rel="nofollow">http://bugs.torproject.org/19969</a><br />
for many more details.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
