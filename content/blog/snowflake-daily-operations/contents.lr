title: Sustaining Snowflake operations
---
author: dcf
---
pub_date: 2023-02-07
---
categories:

circumvention
fundraising
metrics
---
summary: Snowflake bridge operators are raising funds to keep infrastructure running
---
body:

The team that runs the primary Snowflake bridge is raising funds to pay for server operating expenses such as bandwidth, hardware, and maintenance..
You can help the project by donating to the project on Open Collective:

[Snowflake Daily Operations – Open Collective](https://opencollective.com/censorship-circumvention/projects/snowflake-daily-operations)

What follows is [an update originally posted on the Open Collective project](https://opencollective.com/censorship-circumvention/projects/snowflake-daily-operations/updates/2022-year-in-review).

The year 2022 was transformative for the [Snowflake](https://snowflake.torproject.org/) censorship circumvention system. The year saw a massive increase in the number of Snowflake users, fueled by some significant censorship events where Snowflake was one of few systems that worked to keep people connected. At the end of 2022, something around 2% of all Tor users used Snowflake to access the Tor network and the uncensored Internet. The Snowflake team has had to invest in powerful server hardware and a fast network connection in order to keep up with demand. Servers and bandwidth don't come for free, which is why we have started [an Open Collective project](https://opencollective.com/censorship-circumvention/projects/snowflake-daily-operations). We aim to collect funds to sustain current operational needs and support future upgrades.

The first major event affecting Snowflake usage actually happened at the end of 2021. Snowflake is deployed as a circumvention system (a pluggable transport) alongside the [Tor](https://www.torproject.org/) anonymity system. People use Snowflake and other pluggable transports when direct access to the Tor network is blocked. On December 1, 2021, some Internet service providers in Russia suddenly [blocked access](https://blog.torproject.org/tor-censorship-in-russia/) to most ways of accessing Tor, including, briefly, Snowflake. With the help of people in Russia, Snowflake developers [found](https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40014#note_2765074) and [fixed](https://github.com/pion/dtls/pull/410) the protocol flaw that was being used to discover and block Snowflake connections, and Snowflake began working again in Russia. But because other ways of accessing Tor remained blocked, [more and more people in Russia](https://bugs.torproject.org/tpo/community/support/40050#note_2826459) began using Snowflake.

All the new users started to overwhelm the resources of the server that was then hosting the Snowflake bridge. There were times when the system was barely usable because off all the people trying to use it at once. We had to [innovate a new way to deploy a Tor bridge](https://forum.torproject.net/t/tor-relays-how-to-reduce-tor-cpu-load-on-a-single-bridge/1483) to remove a performance bottleneck; then, when that had taken us as far as it could, we [started looking for a more powerful server](https://forum.torproject.net/t/tor-project-more-resources-required-for-snowflake-bridge/2353). We [moved to better hardware](https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40110#note_2791036) in March, which enabled the Snowflake bridge to meet demand. The situation remained stable for about the next six months.

On September 21, 2022, Internet service providers in Iran began imposing [even more severe censorship](https://ooni.org/post/2022-iran-blocks-social-media-mahsa-amini-protests) than usual, in response to mass protests. Evidently, many people in Iran turned to Snowflake to circumvent the blocks, as the number of users quadrupled in a matter of days. This began a few days of [intense performance and optimization work](https://lists.torproject.org/pipermail/anti-censorship-team/2022-October/000270.html) because the greatly increased load was straining even the upgraded server hardware.

The performance work was ultimately successful, and the bridge started handling its new load of users smoothly. Then, about two weeks after the protests began, Snowflake traffic from Iran [dropped off precipitously](https://github.com/net4people/bbs/issues/131). The cause turned out to be an oversight in the implementation of Snowflake, a [TLS fingerprint](https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40207#note_2849437) that has been blocked. When a fix for that problem was deployed, the number of users began to grow again, faster than ever.

Over the course of 2022, Snowflake scaled from 5,000 to 75,000 users. It has been made possible by a dedicated team and investment in hardware infrastructure. As you can see from the user graph, demand for Snowflake continues to grow. It seems likely that there is [additional blocking in Russia](https://bugs.torproject.org/tpo/anti-censorship/censorship-analysis/40030#note_2823140) that has not yet been addressed; and when it is, it is likely to increase usership further.
