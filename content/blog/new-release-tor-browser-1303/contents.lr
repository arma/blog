title: New Release: Tor Browser 13.0.3 (Android)
---
pub_date: 2023-11-06
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.3 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.3/).

This is an emergency release which resolves a critical bug in the tor domain isolation system initialization. Please see [tor-browser#42222](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42222) for more details.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.2](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- Android
  - [Bug tor-browser#42222](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42222): Fix TorDomainIsolator initialization on Android
