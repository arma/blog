title: New Release: Tor Browser 10.5a8
---
pub_date: 2021-01-26
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a8 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a8/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1009">latest stable release</a> instead.</p>
<p>This release updates Firefox to 78.7.0esr for desktop and Firefox for Android to 85.1.0. Additionally, we update Tor to 0.4.5.4-rc. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-04/">security updates</a> to Firefox for Desktop, and similar important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-03/">security updates</a> to Firefox for Android.</p>
<p><b>Note:</b> Default bridges <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40282">were not working</a> in recent Tor Browser Alpha versions and Tor Browser did not start, as a result. The underlying <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40282">issue</a> is now fixed and Tor Browser Alpha should be usable again in that configuration. Sorry for the inconvenience.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a7 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.1.9</li>
<li>Update Tor to 0.4.5.4-rc</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.7.0esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40249">Bug 40249</a>: Remove EOY 2020 Campaign</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40307">Bug 40307</a>: Rebase 10.5 patches onto 78.7.0esr</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 85.1.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40037">Bug 40037</a>: Rebase 10.5 patches onto 70.0.16</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40137">Bug 40137</a>: Remove EOY 2020 Campaign</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40139">Bug 40139</a>: Rebase 10.5 patches onto 85.1.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40305">Bug 40305</a>: Rebase 10.5 patches onto 85.0</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li>Update Go to 1.15.7</li>
<li><a href="https://bugs.torproject.org/33693">Bug 33693</a>: Change snowflake and meek dummy address [tor-browser]</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40208">Bug 40208</a>: Mitigate uniffi non-deterministic code generation</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40112">Bug 40112</a>: Strip libstdc++ we ship</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290994"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290994" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>opaipa (not verified)</span> said:</p>
      <p class="date-time">January 26, 2021</p>
    </div>
    <a href="#comment-290994">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290994" class="permalink" rel="bookmark">Most important:
after 2 non…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Most important:<br />
after 2 non-working versions (10.5a6 and 10.5a7) a working alpha again<br />
most probably due to tor-stack bugs (0.4.5.2-rc and 0.4.5.3-rc)<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40282" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/402…</a><br />
thx</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 28, 2021</p>
    </div>
    <a href="#comment-291018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291018" class="permalink" rel="bookmark">the browser.display.document…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the browser.display.document_color_use config setting is not honored. Hoping you can fix that...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 28, 2021</p>
    </div>
    <a href="#comment-291019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291019" class="permalink" rel="bookmark">https://gitlab.torproject…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40307" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/403…</a><br />
Rebase 10.5 patches onto 78.7.0esr<br />
<a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/56ab7e2d173accfa5cde46659c7b801c8dab2a3e" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/56a…</a><br />
Bug 1655669 - part 2: Install snap binary because it's now required by snapcraft</p>
<p>What does it mean?<br />
Forced to install Snap like *buntu when i install the Torbrowser on Linux not *buntu?<br />
Snap, with autonomous self-updates i can't stop in an easy way.<br />
Is this right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291178"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291178" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291019" class="permalink" rel="bookmark">https://gitlab.torproject…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291178">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291178" class="permalink" rel="bookmark">No, that was a change in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, that was a change in Firefox. Tor Browser is not currently available as a Snap.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291037" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2021</p>
    </div>
    <a href="#comment-291037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291037" class="permalink" rel="bookmark">A Burmese version (မြန်မာစာ,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A Burmese version (မြန်မာစာ, ISO 639-1: my) would help Myanmar. The website is not translated to Burmese either. The font is Myanmar3 or Noto Sans Myanmar. Google Translate and Android since 4.4 (2013) recognize Burmese Unicode.</p>
<p>State-controlled ISPs, dominated by the state-owned <a href="https://en.wikipedia.org/wiki/Myanma_Posts_and_Telecommunications" rel="nofollow">Myanmar Post And Telecommunication</a> (MPT, မြန်မာ့ဆက်သွယ်ရေးလုပ်ငန်း), occasionally apply bandwidth caps. Fortinet, a California-based company, provided the government with internet filtering software in 2005-2006. Prior to 2012, during military rule, SIM cards cost USD 1,500. The major <a href="https://www.submarinecablemap.com/#/landing-point/pyapon-myanmar" rel="nofollow">submarine telecommunication cables</a> come on land at Pyapon, Ngwe Saung, and Letkokkon. Satellite telecoms include Intelsat (Indian Ocean) and ShinSat. The primary <a href="https://www.internetexchangemap.com/#/building/22743" rel="nofollow">internet exchange point</a> (IXP) probably is in Yangon.</p>
<p>* Wikipedia: <a href="https://en.wikipedia.org/wiki/Internet_in_Myanmar" rel="nofollow">Internet in Myanmar</a>, <a href="https://en.wikipedia.org/wiki/Telecommunications_in_Myanmar" rel="nofollow">Telecommunications in Myanmar</a><br />
* <a href="https://map.internetintel.oracle.com/?root=national&amp;country=MM" rel="nofollow">Traceroute, BGP, and DNS query completion</a><br />
* <a href="https://map.internetintel.oracle.com/?root=traffic&amp;country=MM&amp;asn=45558" rel="nofollow">Upstream latency and traceroutes</a><br />
* <a href="https://metrics.torproject.org/userstats-relay-country.html?start=2021-01-24&amp;end=2021-02-07&amp;country=mm&amp;events=on" rel="nofollow">Tor relay users in Myanmar</a><br />
* <a href="https://metrics.torproject.org/userstats-bridge-country.html?start=2021-01-24&amp;end=2021-02-07&amp;country=mm" rel="nofollow">Tor bridge users in Myanmar</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291179"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291179" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">February 17, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291037" class="permalink" rel="bookmark">A Burmese version (မြန်မာစာ,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291179">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291179" class="permalink" rel="bookmark">Thank you for your interest!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your interest! Please help us with localizing:<br />
<a href="https://community.torproject.org/localization/" rel="nofollow">https://community.torproject.org/localization/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
