title: We've Teamed Up With Mullvad VPN to Launch the Mullvad Browser
---
author: isabela
---
pub_date: 2023-04-03
---
categories:

announcements
applications
releases
---
summary: We have partnered with Mullvad VPN to develop the Mullvad Browser - a Tor Browser without Tor
---
body:

Today we announced the launch of the Mullvad Browser, a browser built by the Tor Project team and distributed by [Mullvad](https://mullvad.net/browser).

Mullvad and the Tor Project have been part of the same community that is dedicated to developing technology that prioritizes protecting people's right to privacy for many years now. Mullvad contributes to the Tor Project at the highest level of membership, Shallot, and were a founding member of the Tor Project's Membership Program. They approached us to help them develop their browser because they wanted to leverage our expertise to create a product that is built on the same principles and with similar safety levels as the Tor Browser -- but that works independently of the Tor network. The result is the Mullvad Browser, a free, privacy-preserving web browser to challenge the all-too-prevalent business model of exploiting people's data for profit.

# Why use the Mullvad Browser?

In short: the Mullvad Browser is Tor Browser without the Tor Network -- a browser that allows anyone to take advantage of all the browser privacy features the Tor Project has created. If people want to connect the browser with a VPN they trust, they can easily do so.

Our goal was to give users the privacy protections of Tor Browser without Tor. For instance, the Mullvad Browser applies a "hide-in-the-crowd" approach to online privacy by creating a similar fingerprint for all of its users. The browser's 'out-of-the-box' configurations and settings will mask many parameters and features commonly used to extract information from a person's device that can make them identifiable, including fonts, rendered content, and several hardware APIs. By default, Mullvad Browser has private mode enabled, blocks third-party trackers and cookies, and makes it easy to delete cookies between visiting pages during the same session.

The Mullvad Browser is another option for internet users who are looking for a privacy browser that doesn't need a bunch of extensions and plugins to enhance their privacy and reduce the factors that can accidentally de-anonymize themselves. And unlike other browsers on the market, Mullvad Browser's business model does not rely on capitalizing on users' behavioral data.

# Why collaborate?

Our mission at the Tor Project is to advance human rights by building technology that protects people's privacy, provides anonymity and helps them bypass censorship. We want to give people options and demonstrate to the world that through partnerships like these, you can create technology with these values in mind.

That is why we jumped at the opportunity to help Mullvad with their browser. We agree with them that demand for a browser that is built to the same standards as Tor Browser exists.

We hope to inspire other tech builders and organizations to take a page out of our playbook --and think of privacy as a 'feature' that can enhance user experience-- and not as an afterthought. This collaboration with Mullvad illustrates that it is possible to build privacy-preserving technology that protects users like ours does together, rather than in competition with each other.

# What does that mean for us and Tor Browser?

Let's be clear: Tor Browser is here to stay, and we'll continue to iterate and improve on it and our other services. We know that millions of users around the world rely on Tor Browser and other solutions that the Tor Project offers to safely connect to the internet, to browse anonymously online and to circumvent censorship. Therefore Tor Browser will continue to exist. 

There are a lot of reasons to continue to maintain and improve Tor Browser, it is still one of the few solutions that provides anonymity online because it funnels traffic through the Tor network. A privacy browser plus the Tor network is a powerful combination and sometimes one of the few options that censored and surveilled users have in their region to freely and safely access the internet. Tor Browser is also a free solution for all, making it an affordable solution for people at risk.

This joint project with Mullvad has brought positive changes to Tor Browser by allowing us to address legacy issues, fix vulnerabilities for Tor Browser and make necessary UX improvements that benefit both Tor and Mullvad Browsers, as well as the global privacy-preserving tech ecosystem. And, over the last five years, the Tor Project has launched a number of [initiatives to increase adoption of our technologies](https://wiki.mozilla.org/Security/Tor_Uplift) and made [significant improvements](https://blog.torproject.org/tor-browser-advancing-privacy-innovation/) to the usability of our own products. And we are currently working on more to come!

We encourage you to check out the Mullvad Browser and its capabilities. It can be used without Mullvad VPN, although the combination is recommended. If you want to learn more about this partnership, you can visit [Mullvad.net/browser.](https://mullvad.net/browser)