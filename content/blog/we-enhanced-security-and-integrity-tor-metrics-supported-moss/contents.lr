title: We Enhanced the Security and Integrity of Tor Metrics, Supported by MOSS
---
pub_date: 2017-08-03
---
author: iwakeh
---
tags:

mozilla
metrics
funding
---
categories:

fundraising
metrics
partners
---
_html_body:

<p dir="ltr"> </p>
<p dir="ltr">With the generous support of Mozilla's Open Source Support (MOSS) program's <a href="https://www.mozilla.org/en-US/moss/mission-partners/">Mission Partners</a> track, the Tor Metrics team has spent the last 12 months making improvements to your one-stop shop for Tor data.</p>
<p dir="ltr">As we noted in a blogpost <a href="https://blog.torproject.org/blog/tors-innovative-metrics-program-receives-award-mozilla">announcing</a> the award last June, <a href="https://metrics.torproject.org/">Tor Metrics</a> faces an interesting challenge: how can an anonymity network gather information on its users? It has to be done in a way that’s privacy-preserving and not in a way that puts users at risk.</p>
<p dir="ltr">The MOSS award allowed the team to make several improvements to the security and the integrity of Tor Metrics. In fact, this grant allowed Tor Metrics to become a team, expanding beyond one full-time developer. We made a large number of infrastructure improvements to the Tor Metrics code base, including refactoring the code and adding more tests. In short, this MOSS award made Tor Metrics more mature and gave it the foundation with which to scale.</p>
<p><img alt="Tor Metrics Website Redesign " src="/static/images/blog/inline-images/tor-metrics-screenshot.png" width="400" class="align-center" /></p>
<p dir="ltr">Some of the things the MOSS award allowed us to do:</p>
<ul>
<li dir="ltr">
<p dir="ltr">Develop and strengthen the infrastructure related to data collection, performance measurement, and network status monitoring. Add a feature to synchronize data between CollecTor instances.</p>
</li>
<li dir="ltr">
<p dir="ltr">Analyze and publish a <a href="https://research.torproject.org/techreports/privacy-in-memory-2017-04-28.pdf">report</a> on the security of our metrics and the amount of sensitive, personally identifying data we store, and identify further improvements.</p>
</li>
<li dir="ltr">
<p dir="ltr">Improve the accuracy and depth of performance measurements by adding new <a href="https://metrics.torproject.org/torperf.html?server=onion">onion server measurements</a>.</p>
</li>
<li dir="ltr">
<p dir="ltr">Redesign the <a href="https://metrics.torproject.org/">Tor Metrics</a> website, making it easier to find, compare, and interpret Tor usage and performance metrics, as well as by expanding existing measurements.</p>
</li>
</ul>
<p dir="ltr">For more details, download the <a href="https://trac.torproject.org/projects/tor/raw-attachment/wiki/org/sponsors/SponsorX/Metrics%20Team%20Final%20Report%20to%20MOSS.pdf">PDF</a> of our full report. If you’d like to get involved with Tor Metrics, check out the <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/MetricsTeam">wiki</a> to see how.</p>
<p><img alt="Mozilla Logo" src="/static/images/blog/inline-images/Mozilla-400px_logo.jpg" width="200" class="align-center" /></p>
<p dir="ltr">The work carried out under this MOSS award couldn’t have happened without the support of many teams at Tor and anonymous volunteer cypherpunks.</p>
<p>Mozilla's <a href="https://www.mozilla.org/en-US/mission/">mission</a> is to ensure the Internet is a global public resource, open and accessible to all. <a href="https://wiki.mozilla.org/MOSS">Mozilla Open Source Support</a> is an awards program specifically focused on supporting the Open Source and Free Software movement. Their <a href="https://wiki.mozilla.org/MOSS/Mission_Partners">Mission Partners track</a> is open to any open-source or free software project undertaking an activity which significantly furthers Mozilla's mission. Tor appreciates their support.</p>

---
_comments:

<a id="comment-270148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span># (not verified)</span> said:</p>
      <p class="date-time">August 03, 2017</p>
    </div>
    <a href="#comment-270148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270148" class="permalink" rel="bookmark">I don&#039;t see this as been a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't see this as been a good thing , how is it good me , if there are only 20 people that use my config  'say tor + plugin'  in my  country and you publish it ?  OPSEC Fail ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270186"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270186" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>zorg (not verified)</span> said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270148" class="permalink" rel="bookmark">I don&#039;t see this as been a…</a> by <span># (not verified)</span></p>
    <a href="#comment-270186">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270186" class="permalink" rel="bookmark">They only count how many…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They only count how many people use Tor in your country they don't post anything that identifies those people (IP addresses).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-270190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270190" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270148" class="permalink" rel="bookmark">I don&#039;t see this as been a…</a> by <span># (not verified)</span></p>
    <a href="#comment-270190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270190" class="permalink" rel="bookmark">You don&#039;t see what as a good…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You don't see what as a good thing? Having more people have time to think about our metrics infrastructure, and making it scale, and be better documented about what exactly it publishes? I think "this" is pretty clearly a good thing.</p>
<p>Unless you meant you don't see the whole concept of trying to safely measure Tor's progress as being a good thing. I suggest you read one of our early papers on the topic, published at the "Workshop on Ethics in Computer Security Research":<br />
<a href="https://www.freehaven.net/anonbib/#wecsr10measuring-tor" rel="nofollow">https://www.freehaven.net/anonbib/#wecsr10measuring-tor</a><br />
And you're right that publishing anything precise about a situation where only 20 people have some configuration would be a poor choice. That's why we try not to do things like that -- and that's why we are pleased to have had some funding from Mozilla so we can spend time making sure we're not doing things like that.</p>
<p>Hope this helps. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270151" class="permalink" rel="bookmark">Great work as always and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great work as always and thanks too Mozilla for the support!</p>
<p>I read somewhere that there was going to be more private stats incorporated into Tor? Is that right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270167"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270167" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 05, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270151" class="permalink" rel="bookmark">Great work as always and…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270167">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270167" class="permalink" rel="bookmark">I guess you&#039;re talking about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I guess you're talking about PrivCount being worked on by teor</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270191" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270167" class="permalink" rel="bookmark">I guess you&#039;re talking about…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270191" class="permalink" rel="bookmark">Could be! For more on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could be! For more on Privcount, check out<br />
<a href="https://github.com/privcount/privcount" rel="nofollow">https://github.com/privcount/privcount</a><br />
and<br />
<a href="https://www.nrl.navy.mil/itd/chacs/jansen-safely-measuring-tor" rel="nofollow">https://www.nrl.navy.mil/itd/chacs/jansen-safely-measuring-tor</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-270152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270152" class="permalink" rel="bookmark">1- Is an attacker ( hacker,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1- Is an attacker ( hacker, CIA, governement ... ) able to use this data to compromise a targeted user or Tor network?<br />
2- Tor ( not Tor-Browser-bundle ) collects any data?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270192" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270152" class="permalink" rel="bookmark">1- Is an attacker ( hacker,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270192" class="permalink" rel="bookmark">Re 1, the whole goal is that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Re 1, the whole goal is that the answer should be no. As an example, if you have a data set and you're pondering whether it's safe to publish, consider some hypothetical other party out there that has some other data set, and ask yourself if it's possible that they would learn too much if they combined their hypothetical data set with yours. If the answer is yes, don't publish, or even better, don't collect your data set in the first place.</p>
<p>For more on this topic of minimization, check out<br />
<a href="https://research.torproject.org/safetyboard.html#guidelines" rel="nofollow">https://research.torproject.org/safetyboard.html#guidelines</a></p>
<p>Re 2, Tor Browser doesn't collect any statistics. It's only the relays that collect anonymized, aggregated statistics, and publish them to the world in their "extra-info" descriptors. You can see it all here:<br />
<a href="https://collector.torproject.org/recent/relay-descriptors/extra-infos/" rel="nofollow">https://collector.torproject.org/recent/relay-descriptors/extra-infos/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270153"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270153" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270153">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270153" class="permalink" rel="bookmark">&gt; It has to be done in a way…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; It has to be done in a way that’s privacy-preserving and not in a way that puts users at risk.<br />
Is the Tor Project able to put users at risk at all then?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270189"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270189" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270153" class="permalink" rel="bookmark">&gt; It has to be done in a way…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270189">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270189" class="permalink" rel="bookmark">Well, yes? We could write…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, yes? We could write software that has bugs or mistakes in it, and then when other people run it, it's not as good as it could be.</p>
<p>I hope this is not a deep or surprising concept. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270157"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270157" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270157">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270157" class="permalink" rel="bookmark">Great Work! Does somebody…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great Work! Does somebody know when and if data regarding the new onion services (proposal 224) will be measured?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270188" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270157" class="permalink" rel="bookmark">Great Work! Does somebody…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270188" class="permalink" rel="bookmark">I would guess that we&#039;ll…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would guess that we'll stick to just the current two that we collect:<br />
<a href="https://metrics.torproject.org/hidserv-dir-onions-seen.html" rel="nofollow">https://metrics.torproject.org/hidserv-dir-onions-seen.html</a><br />
But you're right, we might want to start distinguishing between "old style" and "new style" addresses, for the onion address count. (For the "onion traffic" count, I think the relays that collect those stats are not able to distinguish whether they're carrying traffic for legacy onion services or 224-style onion services.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270193" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-270193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270193" class="permalink" rel="bookmark">Another useful link is this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Another useful link is this blog post:<br />
<a href="https://blog.torproject.org/blog/some-statistics-about-onions" rel="nofollow">https://blog.torproject.org/blog/some-statistics-about-onions</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-270184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>keepyourmoney (not verified)</span> said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
    <a href="#comment-270184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270184" class="permalink" rel="bookmark">sedyBRNEF : 1,676 bytes this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>sedyBRNEF : 1,676 bytes this file appeared this day when i launched Tort clicking on the icon ; is it a bug, a survey, or a compromised relay ?<br />
is it related at Tor Metrics ?<br />
(first time i saw that !)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270187" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270184" class="permalink" rel="bookmark">sedyBRNEF : 1,676 bytes this…</a> by <span>keepyourmoney (not verified)</span></p>
    <a href="#comment-270187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270187" class="permalink" rel="bookmark">Sounds unrelated to Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds unrelated to Tor. Which Tor package are you running, and on what platform? Are you sure it's really Tor? What's in the file?</p>
<p>Definitely not a metrics thing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270194"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270194" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>MyNameIsMe (not verified)</span> said:</p>
      <p class="date-time">August 07, 2017</p>
    </div>
    <a href="#comment-270194">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270194" class="permalink" rel="bookmark">Why no pictures appear in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why no pictures appear in facebook account when connect to facebookcorewwwi.onion site? The image placeholder only appears instead with text "image may contain:..."<br />
It's just a Facebook site problem only. Other sites are OK in Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-270196"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270196" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>givebackyourmoney (not verified)</span> said:</p>
      <p class="date-time">August 07, 2017</p>
    </div>
    <a href="#comment-270196">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270196" class="permalink" rel="bookmark">Debian 9.1 stable : linux…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Debian 9.1 stable : linux x86_64 &amp; Tor 7.0.3 (clean, updated, checked, verified &amp; sound/camera off etc.)</p>
<p>it is really in the folder tor-browser_en-US : it is related to Tor.<br />
sedyBRNEF appears between the folder Browser &amp; the launcher Tor Browser.</p>
<p>- can't open the file (no app for that).<br />
- reloading the folder or the tor page has had no effect.</p>
<p>if it is not a metrics thing it could be a Tor bug : "a Tor unknown file ?" _ bizarre.<br />
where could i send a copy of this file if it appears again ?<br />
bug-report ?<br />
what is the signification of sedyBRNEF ? </p>
<p>Torsandbox runs using the terminal so i cannot know if there is interference with Tor browser &amp; if there are 'linked': this "Tor unknown file ?" should look like a leak in this case.</p>
<p>Easter-egg ? i do not thing so, could be the trace of an infiltration/connection test ?<br />
thanks you for your response arma.</p>
</div>
  </div>
</article>
<!-- Comment END -->
