title: Tor and the humans who use it
---
pub_date: 2021-11-17
---
author: alsmith
---
categories: human rights
---
summary: Today, the Tor Project is holding our second annual [State of the Onion](https://blog.torproject.org/state-of-the-onion-2021 "State of the Onion"), an event where Tor teams and the larger Tor community come together to share what we've accomplished this year, and what we're looking forward to in 2022.
---
body:

Today, the Tor Project is holding our second annual [State of the Onion](https://blog.torproject.org/state-of-the-onion-2021 "State of the Onion"), an event where Tor teams and the larger Tor community come together to share what we've accomplished this year, and what we're looking forward to in 2022.

This is an opportunity to talk about the many technical advancements our teams have made this year --- and it's also an opportunity to stop and look at the larger picture. With every release, every UX change, and every new translation, we aim to make it easier for people around the world to access the free and uncensored internet.

When we started [our year-end fundraising campaign](https://blog.torproject.org/privacy-is-a-human-right "our year-end fundraising campaign") --- the theme of which is **Privacy is a Human Right** --- I stopped to think about what privacy means to me. Why does this mission resonate with me?

It goes way back. I grew up in the southern U.S., in the Bible Belt, which is a conservative and strictly religious place. As a queer kid I just did not fit in with this conservative culture.

The internet was a place where I could find out about a world outside of this narrow life experience. I was able to talk to people who were more like me, and it gave me hope and strength to know that a different life was possible.

**But I couldn’t have done this without privacy.** Had these conversations been public with my friends or my family or my peer group, I could have faced some very real negative consequences. The opportunity to explore and research and speak in private is probably what kept me going. In the grand scheme of things, my experience isn’t as high-stakes as many other folks who need privacy, but it’s why this idea --- that everyone on earth has the human right to privacy --- means so much to me.

**And that’s what we’re doing at Tor. We're making privacy possible.**

The best way to demonstrate Tor's impact (and what you're making possible with [your donations](https://torproject.org/donate/donate-phr-bp3 "your donations")) is to share the stories of people who use our tools and how Tor makes it possible for these people to exercise their human right to privacy. I went through the many excellent anonymous submissions to our Tor Stories survey (link) and picked out some stories that demonstrate clearly what Tor makes possible for millions of every day.

## What do you make possible with your donation to Tor?

**Privacy.**

> “I use Tor as [my] everyday browser. Especially when I research doctors and other very personal stuff, it feels better, 'cause hopefully there won't be data for sale, telling the world about my assumed medical condition.”

**Censorship circumvention.**

> “I'm Chinese. In China, Google and Wikipedia are blocked. I can't stand [it]. Sometimes I use Tor to [get] across the GFW...  Tor has provided me with a lot of help.”

**Safety.**
> “I'm from the United States... I am a domestic violence victim...
>
> My husband track[s] my online activity in real time, selectively block[s] my website access, interfere[s] with my online experience, attempt[s] to steal my account credentials, delete[s] data from my devices, and in a multitude of ways violate[s] my right to privacy.
>
> I started using Tor to protect my privacy... If not for Tor, I would not have another option for online privacy.”

**Dissent.**
> “Tor Browser / Orbot / Tails helped me so much during the 2019-2020 Hong Kong protests as a pro-democracy activist in Hong Kong. It allowed me to read / write / organize freely during the protests. [Tor] has become especially important after CCP imposed its National Security Law on Hong Kong, criminalizing political dissent. Tor Browser / Orbot / Tails allows me to continue my political activism and read / write without self-censorship on Twitter using my pseudonymous account.”

**Freedom.**

> “I live in [Central Europe]...
>
> Tor helped me very much. I educated myself without state propaganda. It got me out of the social bubble and I was able to get insights, ideas, and views from many different social groups.
>
> I would never be free without Tor. And I am not talking just about digital freedom. I am talking about physical freedom as well."

And these are just a few examples. Privacy is a human right. Stand up for that right with a donation to Tor. Right now is an excellent time to make a gift, as [all donations are currently matched](https://torproject.org/donate/donate-phr-bp3 "all donations are currently matched"), 1:1, by [Friends of Tor](https://blog.torproject.org/privacy-is-a-human-right "Friends of Tor").
