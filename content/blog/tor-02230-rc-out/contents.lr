title: Tor 0.2.2.30-rc is out
---
pub_date: 2011-07-14
---
author: erinn
---
tags:

tor
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.2.30-rc is the first release candidate for the Tor 0.2.2.x<br />
series. It fixes a few smaller bugs, but generally appears stable.<br />
Please test it and let us know whether it is!  </p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>Changes in version 0.2.2.30-rc - 2011-07-07</strong><br />
  <strong>Minor bugfixes:</strong>                                                                                                                                                 </p>

<ul>
<li>Send a SUCCEEDED stream event to the controller when a reverse<br />
resolve succeeded. Fixes bug 3536; bugfix on 0.0.8pre1. Issue<br />
discovered by katmagic.</li>
<li>Always NUL-terminate the sun_path field of a sockaddr_un before<br />
passing it to the kernel. (Not a security issue: kernels are<br />
smart enough to reject bad sockaddr_uns.) Found by Coverity;<br />
CID #428. Bugfix on Tor 0.2.0.3-alpha.</li>
<li>Don't stack-allocate the list of supplementary GIDs when we're<br />
about to log them. Stack-allocating NGROUPS_MAX gid_t elements<br />
could take up to 256K, which is way too much stack. Found by<br />
Coverity; CID #450. Bugfix on 0.2.1.7-alpha.</li>
<li>Add BUILDTIMEOUT_SET to the list returned by the 'GETINFO<br />
events/names' control-port command. Bugfix on 0.2.2.9-alpha;<br />
fixes part of bug 3465.</li>
<li>Fix a memory leak when receiving a descriptor for a hidden<br />
service we didn't ask for. Found by Coverity; CID #30. Bugfix<br />
on 0.2.2.26-beta.</li>
</ul>

<p>  <strong>Minor features:</strong></p>

<ul>
<li>Update to the July 1 2011 Maxmind GeoLite Country database.</li>
</ul>

